
#include "GuiProc.h"


GuiProc Gui;


GuiProc::GuiProc(void):PageStack(10){

}

int GuiProc::update(void){
    static Page* CurrentPage = NULL;
    Page* next_page_tmp = NULL;

    if(NULL == CurrentPage){
        if(NextPage){
            CurrentPage = NextPage;
            CurrentPage->entry();
        }
    }

    if(CurrentPage){
        /* メニュー実行 */
        Page::e_state ret = CurrentPage->update(&next_page_tmp);
        
        /* 遷移処理 */
        /*  遷移時はCurrentPageをNULLにする */
        if(Page::e_state::CONT == ret){   /* 継続 */
            if(CurrentPage!=next_page_tmp){ /* 継続なのに違うメニューを指定された */
                /* おかしい */
            }
        }else{
          if(Page::e_state::NEXT == ret){
            if(PageStack.back() == next_page_tmp){  /* 遷移先が前のページと同じなら，戻る処理と同じにする。 */
              ret = Page::e_state::BACK;
            }else{
              CurrentPage->exit();
              NextPage = next_page_tmp;
              PageStack.push_back(CurrentPage);    /* 今実行したページをスタックに積む */
              CurrentPage = NULL;
            }
          }
          
          if(Page::e_state::BACK == ret){   /* 戻る */
            CurrentPage->exit();        
            if(!PageStack.empty()){
                NextPage = PageStack.back();     /* 前に実行していたのを次のページとする */
                PageStack.pop_back();                /* 前に実行していたのをpop */
            }else{
              NextPage = CurrentPage;     /* 戻る先がないので同じページにする */
            }
            CurrentPage = NULL;
          }
        }
    }

    return 0;
}

int GuiProc::setPage(Page* next_page){
    NextPage = next_page;
    return 0;
}
