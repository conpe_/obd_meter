/* GUI template */

//#pragma once
#ifndef GUIPROC_H
#define GUIPROC_H

#include "src/Page.h"

/* GUI */
class GuiProc{
public:
    GuiProc(void);
    int update(void);
    int setPage(Page* current_page);
private:
    Page* NextPage;
    std::vector<Page*> PageStack;
};

extern GuiProc Gui;

#endif
