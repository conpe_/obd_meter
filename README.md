# obd_meter

## 概要

* OBD2経由で車両の情報を取得し，M5Stackの画面上に表示するアプリです。
* 車両側にはBluetooth接続のOBD2アダプタ(ELM327)[こういうの(Amazonリンク)](https://www.amazon.co.jp/dp/B07RNHZS44/)を使用します。

## 環境
* M5Stack Grayで動作確認済
* Arduino環境

## 準備
1. OBD2アダプタのMACアドレスを調べてください\
androidスマホアプリ「Bluetooth Mac Address Finder」が使いやすいです。
1. 調べたMACアドレスを，設定メニューから入力して下さい。

