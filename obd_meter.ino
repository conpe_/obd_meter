
#include <M5Stack.h>
#include "src/buzz.h"
#include "src/Indicator.h"
#include "GuiProc.h"
#include "src/PageObj.h"
#include "src/DataMem.h"
#include "src/drvMemEsp32Flash.h"
#include "src/SerialCmd.h"


void SerialTask(void *pvParameters);
void ObdTask(void *pvParameters);
void GuiTask(void *pvParameters);

TaskHandle_t taskHandle[5];


DrvMemEsp32Flash drv_mem_flash(MEM_FILENAME_DEFAULT, MEM_USED_BYTE);

// the setup routine runs once when M5Stack starts up
void setup() {
  
  // initialize the M5Stack object
  M5.begin();

  /*
    Power chip connected to gpio21, gpio22, I2C device
    Set battery charging voltage and current
    If used battery, please call this function in your project
  */
  M5.Power.begin();

  //M5.Power.setPowerBoostOnOff(true);    /* 電源長押しでon/off */
  //M5.Power.setPowerBoostSet(false);     /* ↑に従う */
  M5.Power.setPowerBoostSet(true);      /* 電源短押しでon/off */
  M5.Power.setPowerVin(false);          /* USB取り外し時リセットしない(offになる) */
  M5.Power.setPowerBoostKeepOn(false);  /* IP5306スリープ有効 */
  M5.Power.setVinMaxCurrent(0x01);      /* 100mA充電(デフォルト400mA) */
  M5.Power.setAutoBootOnLoad(false);    /* 負荷接続での自動起動無効 */
  
  buzz.init();

  DataMem::init(&drv_mem_flash);
  /* 初期化チェック */
  uint8_t initialized = false;
  DataMem::read(MEM_INITIALIZED, &initialized);
  if(mem_initialized.Init != initialized){
    DataMem::clearAll();
  }


  /* シリアルコマンド初期化 */
  serial_cmd.init();
  
  M5.Lcd.fillScreen(TFT_BLACK);

  indicator.begin();

  /* 初期ページ設定 */
  Gui.setPage(&page_indicate);

  
  /* BT処理タスク開始 */
  xTaskCreatePinnedToCore(ObdTask, "bttask", 2000, NULL, 1, &taskHandle[0], 1);
  xTaskCreatePinnedToCore(SerialTask, "serialtask", 2000, NULL, 2, &taskHandle[1], 1);
  xTaskCreatePinnedToCore(GuiTask, "guitask", 2000, NULL, 2, &taskHandle[2], 0);        /* OBD通信とGUI処理を分けて高速 */
  
}

// the loop routine runs over and over again forever
void loop(){
  static uint32_t tim_1000ms = 0;

  uint32_t current_tim = millis();

  /* 10msごと処理 */
  buzz.task();

  /* 1000msごと処理 */
  while(tim_1000ms <= current_tim){

    DataMem::save();  /* たまに保存 */
    
    /* 電圧監視 */
    if(0==M5.Power.getBatteryLevel() && !M5.Power.isCharging()){
      M5.Power.powerOFF();
    }
    
    tim_1000ms += 1000;
  }

  M5.update();

  delay(10);
}



void SerialTask(void *pvParameters){
  while(1){
    serial_cmd.task();
    
    delay(10);
  }
}


void ObdTask(void *pvParameters){
  while(1){
    indicator.task();
    
    delay(10);
  }
}

void GuiTask(void *pvParameters){
  while(1){
    Gui.update();
    
    delay(50);
  }
}
