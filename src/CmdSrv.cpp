/*******************************************************************************
 * DataMem
 * 概要：
*******************************************************************************/

#include <string.h>

#include "CmdSrv.h"
#include "CmdSrvDl.h"

#define CMDSRV_RCV_LEN      (32)        /* 最大受信データ数 */

#define CMDSRV_HEADER_RCV 			(0xFBBF)	// ヘッダ
#define CMDSRV_HEADER_RET_OK 		(0xBFFB)	// ACK
#define CMDSRV_HEADER_RET_NG 		(0xCFFC)	// NACK
#define CMDSRV_DATA_LEN_WO_DF		(5)			// DataFieldを除いたパケット長
#define CMDSRV_DATA_LEN_WO_DF_SUM	(4)			// DataFieldとチェックサムを除いたパケット長

// パケット構造
#define E_UP_HEAD_IDX 0
#define E_LO_HEAD_IDX 1
#define E_LO_CMD_IDX 2
#define E_DLC_IDX 3
#define E_DATA_IDX 4


/*
 * コマンド受信処理
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdSrv::notify(uint8_t* rcv_msg, uint8_t rcv_len){
    
    /* コマンドを解析 */

#if 0
	static Clock clk = Clock();
	/* 受信タイムアウト */
	if(E_UP_HEAD_IDX < BufDataIdx){			/* データ受信始まってる */
		if(100 < clk.now()){				/* データ受信始まってから100ms以上経ってる */
			BufDataIdx = E_UP_HEAD_IDX;		/* はじめから */
		}
	}else{									/* ヘッダ受信ならリセット */
		clk.reset();
	}
#endif
	for(int i = 0; i < rcv_len; i++) {

		bool IsDataReceived = false;

		// ヘッダの受信
		if(E_LO_HEAD_IDX >= BufDataIdx){

			// 下位ヘッダ受信
			if(BufDataIdx == E_LO_HEAD_IDX) {
				if (rcv_msg[i] == (CMDSRV_HEADER_RCV & 0x00FF)) {
					BufData[BufDataIdx] = rcv_msg[i];
					BufDataIdx++;
				}
				// 下位が来るはずだけど…
				else {
					// 上位かも？
					BufDataIdx = E_UP_HEAD_IDX;
				}
			}

			// 上位ヘッダ受信
			if (BufDataIdx == E_UP_HEAD_IDX) {
				// 上位ヘッダ受信
				if (rcv_msg[i] == (CMDSRV_HEADER_RCV >> 8)) {
					BufData[BufDataIdx] = rcv_msg[i];
					BufDataIdx++;
				}else{
				}
			}
		}
		// ほかデータの受信
		else {
			// 配列外アクセス防止
			if (BufDataIdx <= BufDataSize) {
				BufData[BufDataIdx] = rcv_msg[i];
			}

			// DLCを用いて受信データサイズを決める。
			if (BufDataIdx == E_DLC_IDX) {
				BufDataDlc = BufData[E_DLC_IDX];
				if (BufDataDlc > CMDSRV_DATA_LEN_MAX) BufDataDlc = CMDSRV_DATA_LEN_MAX;
				BufDataSize = BufDataDlc + CMDSRV_DATA_LEN_WO_DF;
			}

			BufDataIdx++;

			// データサイズ分受信したら終わり。
			if (BufDataIdx >= BufDataSize) {
				IsDataReceived = true;
			}
		}
        
		// チェックサム
		if (IsDataReceived) {
			uint8_t RcvSum = BufData[BufDataDlc + CMDSRV_DATA_LEN_WO_DF_SUM];
			uint8_t CalcSum = calcCheckSum(BufData, BufDataSize - 1);

			if (RcvSum != CalcSum) {		/* チェックサム不一致 */
				IsDataReceived = false;

				/* バッファクリア */
				memset(BufData, 0, CMDSRV_DATA_LEN_MAX);
				BufDataIdx = E_UP_HEAD_IDX;
				BufDataSize = CMDSRV_DATA_LEN_MAX;
				BufDataDlc = CMDSRV_DF_LEN_MAX;
				IsDataReceived = false;
			}
		}
    
		if (IsDataReceived) {
            uint8_t cmd_id = BufData[E_LO_CMD_IDX];
            
            /* デフォルト応答 */
            uint8_t cmd_proc_ret = 1;       /* NG応答 */
            uint8_t retdata_len = BufDataDlc;
			uint8_t RetDataTmp[CMDSRV_DATA_LEN_MAX];
            
            /* コマンドを実行 */
            for(uint8_t src_tbl=0; CmdProc::attached_cmd_num>src_tbl; ++src_tbl){
                if(cmd_id == CmdProc::stCmdProcTbl[src_tbl].CmdId){    /* コマンドID一致 */
                    cmd_proc_ret = CmdProc::stCmdProcTbl[src_tbl].cmdProc->CallBack(&BufData[E_DATA_IDX], BufDataDlc, RetDataTmp, &retdata_len);
                    break;
                }
            }

            /* 応答を返す */
			uint16_t header;
            if(0==cmd_proc_ret){	/* OK */
                header = CMDSRV_HEADER_RET_OK;
            }else{					/* NG */
                header = CMDSRV_HEADER_RET_NG;
            }

			sendPacket(header, cmd_id, RetDataTmp, retdata_len);
            
            
			/* バッファクリア */
			//memset(BufData, 0, CMDSRV_DATA_LEN_MAX);
			BufDataIdx = E_UP_HEAD_IDX;
			BufDataSize = CMDSRV_DATA_LEN_MAX;
			BufDataDlc = CMDSRV_DF_LEN_MAX;
			IsDataReceived = false;
        }
    }

	return 0;
}



uint8_t CmdSrv::send(uint8_t cmd_id, uint8_t* data, uint8_t len){
	return sendPacket(CMDSRV_HEADER_RCV, cmd_id, data, len);       /* 送信 */
}

uint8_t CmdSrv::sendPacket(uint16_t header, uint8_t cmd_id, uint8_t* data, uint8_t len){
	uint8_t send_data[CMDSRV_DATA_LEN_MAX];

	send_data[E_UP_HEAD_IDX] = header >> 8;
	send_data[E_LO_HEAD_IDX] = header & 0x00FF;
	
	send_data[E_LO_CMD_IDX] = cmd_id;
	send_data[E_DLC_IDX] = len;

	memcpy(&send_data[E_DATA_IDX], data, len);
	
	send_data[E_DATA_IDX + len] = calcCheckSum(send_data, CMDSRV_DATA_LEN_WO_DF_SUM + len);	/* サム値をセット */
	

	return write(send_data, CMDSRV_DATA_LEN_WO_DF + len);       /* 送信 */
}


/*
 * チェックサム計算
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdSrv::calcCheckSum(uint8_t* data, uint8_t len){
    uint8_t i;
	uint8_t sum = 0;
	
    for(i = 0; i < len; i++){
		sum += data[i];
	}

	return sum;
}