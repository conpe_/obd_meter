/*******************************************************************************
 * コマンドサーバ データリンク
*******************************************************************************/

#ifndef CMDSRVDL_H
#define CMDSRVDL_H

#include <stdint.h>


/* 受信コマンド コールバック処理 */
class CmdProc{
public:
	CmdProc(void){};
	virtual ~CmdProc(void){};
	/* コールバック関数 */
	/* 問題なければ0を返す */
    virtual uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len) = 0;

	/* テーブル定義 */
	class ST_CMD_PROC{
	public:
		uint16_t CmdId;		// コマンドID
		CmdProc* cmdProc;	// サーバから呼び出される関数用IF
	};

	static const ST_CMD_PROC stCmdProcTbl[];
	static const uint8_t attached_cmd_num;
};


#endif //CMDSRVDL_H
