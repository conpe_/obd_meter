/*******************************************************************************
 * DataMem
 * 概要：
*******************************************************************************/

#include <string.h>
#include "DataMem.h"
#include "DataMemType.h"


DrvMem* DataMem::mem_driver = nullptr;
bool DataMem::dirty = false;

/*
 * 初期化
 * 引数：
 * 戻値：
 * 備考：
 */
void DataMem::init(DrvMem* drv_mem){
    mem_driver = drv_mem;
    mem_driver->begin();
    dirty = false;
}

/*
 * 終了処理
 * 引数：
 * 戻値：
 * 備考：
 */
void DataMem::end(void){
    if(dirty){
        mem_driver->close();      /* closeすることでメモリに書き込む */
        dirty = false;
    }
}

/*
 * 保存
 * 引数：
 * 戻値：
 * 備考：
 */
void DataMem::save(void){
    if(dirty){
        mem_driver->close();      /* closeすることでメモリに書き込む */
        dirty = false;
        mem_driver->begin();
    }
}

/*
 * 全定数を初期化
 * 引数：
 * 戻値：
 * 備考：
 */
void DataMem::clearAll(void){
    uint16_t idx;

    dirty = true;
    
    for(idx=0; MEM_ID_NUM>idx; ++idx){
        
        switch(stMemTable[idx].DataType){
        case MEM_UINT8:
            DataMem::write(stMemTable[idx].DataId, (uint8_t)*(uint8_t*)stMemTable[idx].InitVal);
            break;
        case MEM_UINT16:
            DataMem::write(stMemTable[idx].DataId, (uint16_t)*(uint16_t*)stMemTable[idx].InitVal);
            break;
        case MEM_UINT32:
            DataMem::write(stMemTable[idx].DataId, (uint32_t)*(uint32_t*)stMemTable[idx].InitVal);
            break;
        default:
            /* do nothing */
            break;
        }
    }
}

/*
 * 読み込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::read(uint16_t id, uint8_t* dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT8 != stMemTable[idx].DataType){
        return 4;
    }

    uint8_t dat_tmp;
    uint8_t ret = mem_driver->read(stMemTable[idx].Adrs, (uint8_t*)&dat_tmp, 1);
    
    if(*(uint8_t*)stMemTable[idx].MaxVal < dat_tmp){
        return 3;
    }
    if(*(uint8_t*)stMemTable[idx].MinVal > dat_tmp){
        return 2;
    }

    *dat = dat_tmp;

    return ret;
}

/*
 * 読み込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::read(uint16_t id, uint16_t* dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT16 != stMemTable[idx].DataType){
        return 4;
    }

    uint16_t dat_tmp;
    uint8_t ret = mem_driver->read(stMemTable[idx].Adrs, (uint8_t*)&dat_tmp, 2);
    
    if(*(uint16_t*)stMemTable[idx].MaxVal < dat_tmp){
        return 3;
    }
    if(*(uint16_t*)stMemTable[idx].MinVal > dat_tmp){
        return 2;
    }

    *dat = dat_tmp;

    return ret;
}

/*
 * 読み込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::read(uint16_t id, uint32_t* dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT32 != stMemTable[idx].DataType){
        return 4;
    }
    
    uint32_t dat_tmp;
    uint8_t ret = mem_driver->read(stMemTable[idx].Adrs, (uint8_t*)&dat_tmp, 4);
    
    if(*(uint32_t*)stMemTable[idx].MaxVal < dat_tmp){
        return 3;
    }
    if(*(uint32_t*)stMemTable[idx].MinVal > dat_tmp){
        return 2;
    }

    *dat = dat_tmp;

    return ret;
}

/*
 * 書き込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::write(uint16_t id, uint8_t dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT8 != stMemTable[idx].DataType){
        return 4;
    }
    
    if(*(uint8_t*)stMemTable[idx].MaxVal < dat){
        return 3;
    }
    if(*(uint8_t*)stMemTable[idx].MinVal > dat){
        return 2;
    }

    dirty = true;
    return mem_driver->write(stMemTable[idx].Adrs, &dat, 1);
}

/*
 * 書き込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::write(uint16_t id, uint16_t dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT16 != stMemTable[idx].DataType){
        return 4;
    }

    if(*(uint16_t*)stMemTable[idx].MaxVal < dat){
        return 3;
    }
    if(*(uint16_t*)stMemTable[idx].MinVal > dat){
        return 2;
    }
    
    dirty = true;
    return mem_driver->write(stMemTable[idx].Adrs, (uint8_t*)&dat, 2);
}

/*
 * 書き込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t DataMem::write(uint16_t id, uint32_t dat){
    uint16_t idx;

    if(id2idx(id, &idx)){
        return 5;
    }

    if(MEM_UINT32 != stMemTable[idx].DataType){
        return 4;
    }
    
    if(*(uint32_t*)stMemTable[idx].MaxVal < dat){
        return 3;
    }
    if(*(uint32_t*)stMemTable[idx].MinVal > dat){
        return 2;
    }
    
    dirty = true;
    return mem_driver->write(stMemTable[idx].Adrs, (uint8_t*)&dat, 4);
}

/*
 * IDをテーブルインデックスに変換(検索)
 * 引数：
 * 戻値：
 * 備考： 
 */
uint8_t DataMem::id2idx(uint16_t id, uint16_t *idx){

    bool exist = false;

    for(uint16_t src_idx = 0; MEM_ID_NUM > src_idx; ++src_idx){   
        if(id == stMemTable[src_idx].DataId){
            *idx = src_idx;
            exist = true;
            break;
        }
    }

    return !exist;  /* 存在したら0, 無かったら1 */
}


/* CmdProcIF */
CmdProcDataMemRead cmdproc_datamem_read;
CmdProcDataMemWrite cmdproc_datamem_write;
CmdProcDataMemClearAll cmdproc_datamem_clearall;


/*
 * コマンド 読み込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdProcDataMemRead::CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    uint8_t readdata1;
    uint16_t readdata2;
    uint32_t readdata4;
    uint16_t id = (uint16_t)rcv_msg[1]<<8 | (uint16_t)rcv_msg[0];
    
    memcpy(ret_data, rcv_msg, rcv_len);    /* 内容そのまま返す */
    *ret_len = rcv_len;
    
    uint16_t idx;

    if(DataMem::id2idx(id, &idx)){
        return 1;
    }

    /* アクセス制限 */
    if((DATAMEM_PERMISSION_WRITE_ONLY == stMemTable[idx].ExternalAccessPermission) || (DATAMEM_PERMISSION_NONE == stMemTable[idx].ExternalAccessPermission)){
        return 1;
    }

    switch(stMemTable[idx].DataType){
    case MEM_UINT8:
        *ret_len = 2 + 1;
        if(DataMem::read(id, &readdata1)){
            return 1;
        }
        ret_data[2] = readdata1;
        break;
    case MEM_UINT16:
        *ret_len = 2 + 2;
        if(DataMem::read(id, &readdata2)){
            return 1;
        }
        ret_data[3] = readdata2>>8;
        ret_data[2] = readdata2;
        break;
    case MEM_UINT32:
        *ret_len = 2 + 4;
        if(DataMem::read(id, &readdata4)){
            return 1;
        }
        ret_data[5] = readdata4>>24;
        ret_data[4] = readdata4>>16;
        ret_data[3] = readdata4>>8;
        ret_data[2] = readdata4;
        break;
    default:
        return 1;
    }
    
    return 0;
}

/*
 * コマンド 書き込み
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdProcDataMemWrite::CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    uint16_t id = (uint16_t)rcv_msg[1]<<8 | (uint16_t)rcv_msg[0];
    
    memcpy(ret_data, rcv_msg, rcv_len);
    *ret_len = rcv_len;

    uint16_t idx;
    if(DataMem::id2idx(id, &idx)){
        return 1;
    }
    
    /* アクセス制限 */
    if((DATAMEM_PERMISSION_READ_ONLY == stMemTable[idx].ExternalAccessPermission) || (DATAMEM_PERMISSION_NONE == stMemTable[idx].ExternalAccessPermission)){
        return 1;
    }

    switch(stMemTable[idx].DataType){
    case MEM_UINT8:
        if(DataMem::write(id, (uint8_t)rcv_msg[2])){
            return 1;
        }
        break;
    case MEM_UINT16:
        if(DataMem::write(id, (uint16_t)((uint16_t)rcv_msg[3]<<8 | (uint16_t)rcv_msg[2]))){
            return 1;
        }
        break;
    case MEM_UINT32:
        if(DataMem::write(id, (uint32_t)((uint32_t)rcv_msg[5]<<24 | (uint32_t)rcv_msg[4]<<16 | (uint32_t)rcv_msg[3]<<8 | (uint32_t)rcv_msg[2]))){
            return 1;
        }
        break;
    default:
        return 1;
    }
    
    
    return 0;
}
/*
 * 初期化
 * 引数：
 * 戻値：
 * 備考：
 */
uint8_t CmdProcDataMemClearAll::CallBack(uint8_t* rcv_data, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len){
    DataMem::clearAll();
    *ret_len = 0;
    return 0;
}

