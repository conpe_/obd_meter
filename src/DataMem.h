/*******************************************************************************

*******************************************************************************/

#ifndef DATAMEM_H
#define DATAMEM_H

#include <stdint.h>

#include "DataMemList.h"

#include "drvMem.h"


class DataMem{
public:
    static void init(DrvMem* drv_mem);
    static void end(void);
    static void clearAll(void);
    static void save(void);

    /* 読み込み */
    /* データ型を間違えないように！ */
    /* エラーも返さないので注意 */
    static uint8_t read(uint16_t id, uint8_t* dat);     /* 1バイト型 */
    static uint8_t read(uint16_t id, uint16_t* dat);    /* 2バイト型 */
    static uint8_t read(uint16_t id, uint32_t* dat);    /* 4バイト型 */

    /* 書き込み */
    /* データ型を間違えないように！ */
    /* エラーも返さないので注意 */
    static uint8_t write(uint16_t id, uint8_t dat);  /* 1バイト型 */
    static uint8_t write(uint16_t id, uint16_t dat); /* 2バイト型 */
    static uint8_t write(uint16_t id, uint32_t dat); /* 4バイト型 */

    static uint8_t id2idx(uint16_t id, uint16_t *idx);   /* コマンドサーバでも使うので公開 */
private:
    static DrvMem* mem_driver;
    static bool dirty;              /* 書き込みあり */
};


#include "CmdSrvDl.h"
class CmdProcDataMemRead:public CmdProc{
public:
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
};
class CmdProcDataMemWrite:public CmdProc{
public:
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
};
class CmdProcDataMemClearAll:public CmdProc{
public:
    uint8_t CallBack(uint8_t* rcv_msg, uint8_t rcv_len, uint8_t* ret_data, uint8_t* ret_len);
};

extern CmdProcDataMemRead cmdproc_datamem_read;
extern CmdProcDataMemWrite cmdproc_datamem_write;
extern CmdProcDataMemClearAll cmdproc_datamem_clearall;

#endif //DATAMEM_H
