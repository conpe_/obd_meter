/**************************************************
	DataMemList.cpp
	EEPROMメモリ管理
				2021/05/05 21:47:41
**************************************************/

#include "DataMemList.h"


/* EEPROM変数 */
stMem<uint8_t> mem_initialized = {1, 0, 1};		/* EEPROM初期化済みフラグ */
stMem<uint8_t> mem_vel_max = {0, 0, 5};		/* 速度メーター最大値 */
stMem<uint8_t> mem_bt0 = {0, 0, 255};		/* BT接続先MACアドレス0 */
stMem<uint8_t> mem_bt1 = {13, 0, 255};		/* BT接続先MACアドレス1 */
stMem<uint8_t> mem_bt2 = {24, 0, 255};		/* BT接続先MACアドレス2 */
stMem<uint8_t> mem_bt3 = {0, 0, 255};		/* BT接続先MACアドレス3 */
stMem<uint8_t> mem_bt4 = {0, 0, 255};		/* BT接続先MACアドレス4 */
stMem<uint8_t> mem_bt5 = {1, 0, 255};		/* BT接続先MACアドレス5 */


/* EEPROM管理テーブル */
const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN],		DataId,		Adrs,		DataType,		InitVal,		MinVal,		MaxVal,		ExternalAccessPermission,		*/
	{	"", MEM_INITIALIZED,		0x0000,		MEM_UINT8,		&mem_initialized.Init,		&mem_initialized.Min,		&mem_initialized.Max,		DATAMEM_PERMISSION_READ_ONLY,		},		/* EEPROM初期化済みフラグ (Resolution=1, Offset=0) */
	{	"vel_max", MEM_VEL_MAX,		0x0001,		MEM_UINT8,		&mem_vel_max.Init,		&mem_vel_max.Min,		&mem_vel_max.Max,		DATAMEM_PERMISSION_FULL,		},		/* 速度メーター最大値 (Resolution=1, Offset=0) */
	{	"bt_adrs_0", MEM_BT0,		0x0002,		MEM_UINT8,		&mem_bt0.Init,		&mem_bt0.Min,		&mem_bt0.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス0 (Resolution=1, Offset=0) */
	{	"bt_adrs_1", MEM_BT1,		0x0003,		MEM_UINT8,		&mem_bt1.Init,		&mem_bt1.Min,		&mem_bt1.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス1 (Resolution=1, Offset=0) */
	{	"bt_adrs_2", MEM_BT2,		0x0004,		MEM_UINT8,		&mem_bt2.Init,		&mem_bt2.Min,		&mem_bt2.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス2 (Resolution=1, Offset=0) */
	{	"bt_adrs_3", MEM_BT3,		0x0005,		MEM_UINT8,		&mem_bt3.Init,		&mem_bt3.Min,		&mem_bt3.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス3 (Resolution=1, Offset=0) */
	{	"bt_adrs_4", MEM_BT4,		0x0006,		MEM_UINT8,		&mem_bt4.Init,		&mem_bt4.Min,		&mem_bt4.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス4 (Resolution=1, Offset=0) */
	{	"bt_adrs_5", MEM_BT5,		0x0007,		MEM_UINT8,		&mem_bt5.Init,		&mem_bt5.Min,		&mem_bt5.Max,		DATAMEM_PERMISSION_FULL,		},		/* BT接続先MACアドレス5 (Resolution=1, Offset=0) */
};

/* ・Name無しは表示しない */
/* ・DataIdの上位1byteがグループID */

/* グループ管理テーブル */
const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM] = {
	/*	Name[PARAM_NAME_MAXLEN]	*/
	{	"Util"	},		/* Group0 Util */
	{	"Meter"	},		/* Group1 Meter */
	{	"BtAdrs"	},		/* Group2 BtAdrs */
};
