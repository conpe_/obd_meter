/**************************************************
	DataMemList.h
	EEPROMメモリ管理
				2021/05/05 21:47:41
**************************************************/

#ifndef __DATAMEMLIST_H__
#define __DATAMEMLIST_H__

#include "stdint.h"
#include "DataMemType.h"

/* EEPROM容量 [byte] */
#define MEM_CAPACITY 1200	/* 容量(byte) */

/* EEPROM使用量 [byte] */
#define MEM_USED_BYTE 8	/* 使用量(byte) */

/* EEPROM使用ID数 */
#define MEM_ID_NUM 8	/* 使用ID数 */

/* グループ数 */
#define MEM_GROUP_NUM 3	/* グループ数 */



/* EEPROM ID */
#define	MEM_INITIALIZED	0x0000	/* EEPROM初期化済みフラグ */
#define	MEM_VEL_MAX	0x0100	/* 速度メーター最大値 */
#define	MEM_BT0	0x0200	/* BT接続先MACアドレス0 */
#define	MEM_BT1	0x0201	/* BT接続先MACアドレス1 */
#define	MEM_BT2	0x0202	/* BT接続先MACアドレス2 */
#define	MEM_BT3	0x0203	/* BT接続先MACアドレス3 */
#define	MEM_BT4	0x0204	/* BT接続先MACアドレス4 */
#define	MEM_BT5	0x0205	/* BT接続先MACアドレス5 */


/* EEPROM変数 */
extern stMem<uint8_t> mem_initialized;		/* EEPROM初期化済みフラグ */
extern stMem<uint8_t> mem_vel_max;		/* 速度メーター最大値 */
extern stMem<uint8_t> mem_bt0;		/* BT接続先MACアドレス0 */
extern stMem<uint8_t> mem_bt1;		/* BT接続先MACアドレス1 */
extern stMem<uint8_t> mem_bt2;		/* BT接続先MACアドレス2 */
extern stMem<uint8_t> mem_bt3;		/* BT接続先MACアドレス3 */
extern stMem<uint8_t> mem_bt4;		/* BT接続先MACアドレス4 */
extern stMem<uint8_t> mem_bt5;		/* BT接続先MACアドレス5 */


/* EEPROM管理テーブル */
extern const ST_MEM_TABLE_T stMemTable[MEM_ID_NUM];
/* グループ管理テーブル */
extern const ST_MEM_GROUP_TABLE_T stMemGroupTable[MEM_GROUP_NUM];


#endif

