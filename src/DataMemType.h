/**************************************************
	DataMemType.h
	EEPROMメモリ管理
				2021/05/05 21:47:41
**************************************************/

#ifndef __DATAMEMTYPE_H__
#define __DATAMEMTYPE_H__

#include "stdint.h"


/* EEPROMデータ型 */
template<typename T>
class stMem{
public:
	const T Init;
	const T Min;
	const T Max;
};



/* アクセス権 */
#define	DATAMEM_PERMISSION_FULL	0
#define	DATAMEM_PERMISSION_READ_ONLY	1
#define	DATAMEM_PERMISSION_WRITE_ONLY	2
#define	DATAMEM_PERMISSION_NONE	3


/* データ型 */
typedef enum EN_DATAMEM_TYPE{
	MEM_UINT8,
	MEM_UINT16,
	MEM_UINT32
} EN_DATAMEM_TYPE;

/* データ管理テーブル */
#define PARAM_NAME_MAXLEN	(32)
class ST_MEM_TABLE_T{
public:
	char Name[PARAM_NAME_MAXLEN];
	uint16_t DataId;
	uint16_t Adrs;
	EN_DATAMEM_TYPE DataType;
	const void* InitVal;
	const void* MinVal;
	const void* MaxVal;
	uint8_t ExternalAccessPermission;
};

/* グループ管理テーブル */
typedef struct ST_MEM_GROUP_TABLE_T{
	char Name[PARAM_NAME_MAXLEN];
} ST_MEM_GROUP_TABLE_T;

#endif

