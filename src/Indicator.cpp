
#include <Arduino.h>

#include "Indicator.h"

#include "DataMem.h"
#include "elm327.h"


Indicator indicator;



Indicator::Indicator(void):
    BtConnect(0)
{
    
}


uint8_t Indicator::begin(void){

    uint8_t sens_address[6] = {0x00, 0x0D, 0x18, 0x00, 0x00, 0x01};
    
    DataMem::read(MEM_BT0, &sens_address[0]);
    DataMem::read(MEM_BT1, &sens_address[1]);
    DataMem::read(MEM_BT2, &sens_address[2]);
    DataMem::read(MEM_BT3, &sens_address[3]);
    DataMem::read(MEM_BT4, &sens_address[4]);
    DataMem::read(MEM_BT5, &sens_address[5]);
    
    uint8_t ret = obd.begin(sens_address);



    return ret;
}

/* 10ms周期タスク */
void Indicator::task(void){

    /* 接続処理，データ更新 */
    obd.task();


    bool con = obd.isConnected();
    
    if(!con){           /* 繋がってない */
        BtConnect = false;
    }
    if(con && !BtConnect){  /* 繋がった */
        BtConnect = true;
    }


    /* 100msごとに実行 */
    if(BtConnect){

    }
}

