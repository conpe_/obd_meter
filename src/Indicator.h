//#pragma once
#ifndef INDICATOR_H
#define INDICATOR_H

#include "obd2.h"

/* ラップタイム計測処理 */
class Indicator{
public:
    Indicator(void);

    uint8_t begin(void);     /* 初期化 */
    void task(void);

    bool isAvailable(void){return BtConnect;};

private:
    bool BtConnect;
};

extern Indicator indicator;


#endif
