/* ページのテンプレート */

#include <M5Stack.h>
#include "Page.h"
#include "buzz.h"
#include "Indicator.h"




#define CHAR_W 15        // 1文字の幅

#define LCD_BUTTON_LONGPRESSTIME 1500    /* 長押し判定時間[ms] */


uint8_t Page::guruguru_cnt = 0;
bool Page::blink;
uint32_t Page::blink_time;

/* コンストラクタ */
Page::Page(const char* title):button(PAGE_BUTTON_NUM){
    this->title = title;   /* タイトルをセット */
    button[0] = new DispButton("A", BUTTON_A_PIN, true, DEBOUNCE_MS);
    button[1] = new DispButton("B", BUTTON_B_PIN, true, DEBOUNCE_MS);
    button[2] = new DispButton("C", BUTTON_C_PIN, true, DEBOUNCE_MS);

    button[0]->setPos(25, LCD_H-BUTTON_H, BUTTON_W, BUTTON_H);
    button[1]->setPos(LCD_W/2-BUTTON_W/2, LCD_H-BUTTON_H, BUTTON_W, BUTTON_H);
    button[2]->setPos(LCD_W-25-BUTTON_W, LCD_H-BUTTON_H, BUTTON_W, BUTTON_H);


}
/* デストラクタ */
Page::~Page(void){
    for(int i=0; PAGE_BUTTON_NUM>i; ++i){
        delete button[i];
    }
}

/* メニュー入ったときの処理 */
int Page::entry(void){

    M5.Lcd.fillScreen(TFT_BLACK);

    /* タイトル */
    dispMenuTitle();

    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->draw();
    }
    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->read();
    }

    entryProc();

    return 0;
}

/* メニューを抜けるときの処理 */
int Page::exit(void){
    exitProc();

    return 0;
}

/* ページ更新 */
Page::e_state Page::update(Page** next_page){
    /* メニュークリア */

    /* ボタン更新 */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        (*itr)->read();

        /* 押したら鳴らす */
        if((*itr)->wasPressed()){
            //buzz.noteSingle(BUZZ_NOTE::BUZZ_NOTE_C4);
            M5.Speaker.tone(800, 100);
        }
    }

    
    ++guruguru_cnt;
    if(guruguru_cnt>3){
        guruguru_cnt = 0;
    }

    /* 点滅カウント */
    if(blink_time + 500 < millis()){
        blink = !blink;
        blink_time = millis();
    }
    /* ボタン3つ */
    for(auto itr = button.begin(); itr != button.end(); ++itr){
        if((*itr)->wasPressed()){
            blink = 0;
            blink_time = millis();
        }
    }


    /* 各メニューの描画処理 */
    Page::e_state ret = updateProc(next_page);
    
    /* メニュー共通部の描画 */
    dispMenuFormat();

    return ret;
}

int Page::dispMenuTitle(void){
    /* メニュータイトル */
    #define GRAD 30
    M5.Lcd.fillRect(0, 0, LCD_W, 28, 0x000000);
    for(int i=0; GRAD>i; ++i){
        M5.Lcd.fillRect((LCD_W/GRAD)*i, 0, LCD_W/GRAD, 28, i<(GRAD*2/5)?BLUE:BLUE/(i-GRAD*2/5+1));
    }
    M5.Lcd.setTextFont(4);
    M5.Lcd.setTextSize(1);
    M5.Lcd.setTextColor(WHITE);
    M5.Lcd.setCursor(5, 2);
    M5.Lcd.printf("%s", title.c_str());

    return 0;
}

int Page::dispMenuFormat(void){

    /* メニューバー */
    /* バッテリ状態 */
    dispBattState();
    /* OBD接続状態 */
    dispAntenna(indicator.isAvailable()?3:0);
    
    return 0;
}



Page::DispButton::DispButton(const char* label, uint8_t pin, uint8_t invert, uint32_t dbTime):
        Button(pin, invert, dbTime){
    
    setLabel(label);
}

int Page::DispButton::setLabel(const char* label){
    this->label = label;   /* タイトルをセット */

    return 0;
}

int Page::DispButton::draw(void){

    /* ボタン */
    M5.Lcd.fillRect(_x, _y, _w, _h, YELLOW);
    M5.Lcd.drawRect(_x, _y, _w, _h, RED);
    /* ボタン文字 */
    M5.Lcd.setTextFont(4);
    M5.Lcd.setTextColor(RED);
    M5.Lcd.setTextSize(1);
    M5.Lcd.setCursor(_x + _w/2 - (label.length()*CHAR_W/2), _y + 1);

    M5.Lcd.printf("%s", label.c_str());

    return 0;
}

int Page::DispButton::setPos(int x, int y, int w, int h){
    _x=x;
    _y=y;
    _w=w;
    _h=h;

    return 0;
}

bool Page::DispButton::isLongPress(void){
    return pressedFor(1500);    /* 1500ms以上押しっぱなし */
}



/* バッテリ状態 */
void Page::dispBattState(void){
    int8_t soc = M5.Power.getBatteryLevel();
    bool charging = M5.Power.isCharging();

    // 上帯：高さ28
    uint8_t bat_level;
    if(100<=soc){
        bat_level = 4;
    }else if(75<=soc){
        bat_level = 3;
    }else if(50<=soc){
        bat_level = 2;
    }else if(25<=soc){
        bat_level = 1;
    }else{
        bat_level = 0;
    }
    /* チャージ済みセル：緑 */
    /* チャージ中セル：緑点滅 */
    /* 未チャージセル：灰色 */
    for(int cell=0; 4>cell; ++cell){
        M5.Lcd.fillRect(285,4+(3-cell)*5, 10, 4, 
            (bat_level>cell)?TFT_GREEN:(charging&&(bat_level==cell&&(blink))?TFT_GREEN:TFT_DARKGREY)
        );
    }
    /* バッテリ枠：灰色 */
    M5.Lcd.drawRect(284, 3, 12, 21, TFT_LIGHTGREY);
    M5.Lcd.drawRect(288, 1, 4, 2, TFT_LIGHTGREY);

}


/* センサ接続状態 */
void Page::dispAntenna(uint8_t bari){
    static uint8_t bari_last_10 = 0;

    if(bari*10 > bari_last_10){
        bari_last_10 += 3;
    }else if(bari*10 < bari_last_10){
        bari_last_10 -= 3;
    }else{
        bari_last_10 = bari*10;
    }


    M5.Lcd.fillRect(304, 5, 15, 16, TFT_BLACK);
    M5.Lcd.fillRect(305, 6, 13, 3, (bari_last_10/10)>=3?TFT_GREEN:TFT_LIGHTGREY);
    M5.Lcd.fillRect(308, 12, 7, 3, (bari_last_10/10)>=2?TFT_GREEN:TFT_LIGHTGREY);
    M5.Lcd.fillRect(310, 18, 3, 3, (bari_last_10/10)>=1?TFT_GREEN:TFT_LIGHTGREY);

    /* バリ0ならバッテン */
    if(10 > bari_last_10){
        M5.Lcd.drawLine(306,6,317,19, TFT_RED);
        M5.Lcd.drawLine(306,7,317,20, TFT_RED);
        M5.Lcd.drawLine(305,7,316,20, TFT_RED);
        M5.Lcd.drawLine(316,6,305,19, TFT_RED);
        M5.Lcd.drawLine(317,6,305,20, TFT_RED);
        M5.Lcd.drawLine(317,7,306,20, TFT_RED);
    }

}
