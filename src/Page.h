/* GUI template */

//#pragma once

#ifndef PAGE_H
#define PAGE_H

#include <Arduino.h>
#include <M5Stack.h>
#include <utility/Button.h>

#include <string>
#include <vector>
using namespace std;

/* 注意 */
/* M5stackに依存 */
/* どこかでM5.update()が実行されていること */

#define LCD_W 320
#define LCD_H 240
#define PAGE_BUTTON_NUM 3

#define SCROLLBAR_POS_TOP 40
#define SCROLLBAR_H 168
#define SCROLLBAR_W 6

#define BUTTON_W 80
#define BUTTON_H 22

/* ページのテンプレート */
class Page{
public:
    string title;
    Page(const char* title = "MENU");
    virtual ~Page(void);

    enum e_state{
        CONT,
        BACK,
        NEXT
    };

    /* メニュー入ったときの処理 */
    virtual int entry(void);
    /* メニュー処理(ここは継承しない) */
    e_state update(Page** next_page);       /* 継続，戻る，進む(指定メニューへ) */
    /* メニューを抜けるときの処理 */
    virtual int exit(void);


    /* ボタン */
    class DispButton:public Button{
    public:
        DispButton(const char* label, uint8_t pin, uint8_t invert, uint32_t dbTime);
        int draw(void);
        int setLabel(const char* label);
        int setPos(int x, int y, int w, int h);

        bool isLongPress(void);
    protected:
        string label;
        int _x,_y,_w,_h;
    };

    vector<DispButton*> button;   /* ボタン3つ */


protected:
    /* メニュー入ったときの処理 */
    virtual int entryProc(void) = 0;
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page) = 0;
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void) = 0;

    virtual int dispMenuTitle(void);
    virtual int dispMenuFormat(void);                   /* 共通の描画 */

    static void dispSector(void);               /* セクター表示 */
    static void dispMesState(void);             /* 計測状態 */
    static void dispSdState(void);              /* SDカード挿入状態 */
    static void dispBattState(void);            /* バッテリ状態 */
    static void dispAntenna(uint8_t bari);      /* 接続状態表示 */

    static uint8_t guruguru_cnt;
    static bool blink;
    static uint32_t blink_time;
};

#endif
