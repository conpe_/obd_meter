#include "PageIndicate.h"
#include "Indicator.h"
#include "PageObj.h"


PageIndicate::PageIndicate(const char* title):
    Page(title),
    ThrottleBar(), RevBar(),
    th_min(TH_MIN_DEFAULT),
    th_max(TH_MAX_DEFAULT)
{
    
    button[0]->setLabel("<");
    button[1]->setLabel("Menu");
    button[2]->setLabel(">");

    ThrottleBar.setPos(20,55,50,130);
    ThrottleBar.setColor(GREEN);
    ThrottleBar.setMax(100);

    RevBar.setPos(85,55,50,130);
    RevBar.setColor(RED);
    RevBar.setMax(255);

    LoadBar.setPos(150,55,50,130);
    LoadBar.setColor(BLUE);
    LoadBar.setMax(100);

}

PageIndicate::~PageIndicate(void){

}

/* メニュー入ったときの処理 */
int PageIndicate::entryProc(void){
    

    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);
    M5.Lcd.setCursor(21, 38);
    M5.Lcd.printf("Acc");

    M5.Lcd.setCursor(86, 38);
    M5.Lcd.printf("Rev");

    M5.Lcd.setCursor(151, 38);
    M5.Lcd.printf("Load");



    /* 受信したいデータをセット */
    spd = obd.registerPid(0x010D);
    rev = obd.registerPid(0x010C);
    throttle = obd.registerPid(0x0149);
    throttle_cmd = obd.registerPid(0x014C);
    coolant_temp = obd.registerPid(0x0105);
    eng_load = obd.registerPid(0x0104);

    return 0;
}

/* メニューを抜けるときの処理 */
int PageIndicate::exitProc(void){

    /* OBDデータ受信登録解除 */
    obd.unregisterPid(spd);
    obd.unregisterPid(rev);
    obd.unregisterPid(throttle);
    obd.unregisterPid(throttle_cmd);
    obd.unregisterPid(coolant_temp);
    obd.unregisterPid(eng_load);

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageIndicate::updateProc(Page** next_page){

    // 状態遷移
    // left
    if(button[0]->wasPressed()){
        *next_page = &page_indicate_meter;   /* メーター画面 */
        return Page::e_state::NEXT;
    }
    
    // center
    if(button[1]->wasPressed()){
        *next_page = &page_menu;   /* 設定画面へ */
        return Page::e_state::NEXT;
    }

    // right
    if(button[2]->wasPressed()){
        *next_page = &page_indicate_meter;   /* メーター画面 */
        return Page::e_state::NEXT;
    }


    /* 表示 */
    uint8_t throttle_disp = 0;
    uint16_t rev_disp = 0;
    uint8_t spd_disp = 0;
    int16_t cool_disp = 0;
    uint8_t th_cmd_disp = 0;
    uint8_t eng_load_disp = 0;
    
    /* データ取得 */
    if(indicator.isAvailable()){
        /* アクセル開度 */
        /* 0x0111:(ThrottlePos)荒い気がする */
        /* 0x0145:(RelativeThrottlePos)実際のスロットル量？アイドリング高いとき5, 低いとき2～3 */
        /* 0x0147:(AbsoluteThrottlePos) */
        /* 0x0149:(Accelerator pedal position D)ペダル(踏み白ぶんも動く)(51～234) */
        uint8_t t_tmp = 0;
        t_tmp = throttle->at(0);
        if(TH_MIN > t_tmp){
            t_tmp = TH_MIN;
        }else if(TH_MAX < t_tmp){
            t_tmp = TH_MAX;
        }
        /* 上下限更新 */
        if(th_min > t_tmp){
            th_min = t_tmp;
        }else if(th_max < t_tmp){
            th_max = t_tmp;
        }
        throttle_disp = (t_tmp - th_min) * 100 / (th_max - th_min);

        /* エンジン回転数 */
        rev_disp = ((uint16_t)rev->at(0)<<8 | (uint16_t)rev->at(1))/4;

        /* 速度 */
        spd_disp = spd->at(0);

        /* 冷却水温 */
        cool_disp = (int16_t)(coolant_temp->at(0)) - 40;

        /* スロットル */
        th_cmd_disp = throttle_cmd->at(0)*100/255;

        /* エンジン負荷 */
        eng_load_disp = eng_load->at(0)*100/255;
    }


    /* 描画 */
    ThrottleBar.setVal((uint8_t)throttle_disp);
    ThrottleBar.draw();
    RevBar.setVal((uint8_t)((float)rev_disp*255.0f/9000.0f));
    RevBar.draw();
    LoadBar.setVal(eng_load_disp);
    LoadBar.draw();

    

    /* 数値表示 */
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);
    M5.Lcd.setCursor(21, 190);
    M5.Lcd.printf("%4d", throttle_disp);

    M5.Lcd.setCursor(86, 190);
    M5.Lcd.printf("%4d", rev_disp);

    M5.Lcd.setCursor(151, 190);
    M5.Lcd.printf("%4d", eng_load_disp);

    /* 速度 */
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(210, 60);
    M5.Lcd.printf("spd:");
    M5.Lcd.setCursor(215, 80);
    M5.Lcd.printf("%3d km/h", spd_disp);

    /* 冷却水温 */
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(210, 100);
    M5.Lcd.printf("cool:");
    M5.Lcd.setCursor(215, 120);
    M5.Lcd.printf("%3d deg", cool_disp);

    /* スロットル指令 */
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(2);
    M5.Lcd.setCursor(210, 140);
    M5.Lcd.printf("thr:");
    M5.Lcd.setCursor(215, 160);
    M5.Lcd.printf("%3d %%", th_cmd_disp);


    return CONT;
}
