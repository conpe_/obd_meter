/* GUI */

//#pragma once
#ifndef PAGE_INDICATE_H
#define PAGE_INDICATE_H

#include "Page.h"
#include "drawitem_verticalbar.h"
#include "elm327.h"

/*  */
class PageIndicate:public Page{
public:
    PageIndicate(const char* title = "LAP TIMER");
    ~PageIndicate(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);   /* こっちを継承 */

private:
    /* 描画図形 */
    bar_vertical ThrottleBar;
    bar_vertical RevBar;
    bar_vertical LoadBar;

    /* データ */
    ObdData* spd;
    ObdData* rev;
    ObdData* throttle;
    ObdData* throttle_cmd;
    ObdData* coolant_temp;
    ObdData* eng_load;
    
    const uint8_t TH_MIN = 50;
    const uint8_t TH_MAX = 240;
    const uint8_t TH_MIN_DEFAULT = 52;
    const uint8_t TH_MAX_DEFAULT = 233;

    uint8_t th_min;
    uint8_t th_max;
};




#endif
