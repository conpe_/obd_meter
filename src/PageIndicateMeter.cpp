#include "PageIndicateMeter.h"
#include "Indicator.h"
#include "PageObj.h"

#include "DataMem.h"

PageIndicateMeter::PageIndicateMeter(const char* title):
    Page(title),
    SpdMeter()
{
    
    button[0]->setLabel("<");
    button[1]->setLabel("Menu");
    button[2]->setLabel(">");

}

PageIndicateMeter::~PageIndicateMeter(void){

}

/* メニュー入ったときの処理 */
int PageIndicateMeter::entryProc(void){
    
    /* 受信したいデータをセット */
    spd = obd.registerPid(0x010D);

    /* メーター設定 */
    SpdMeter.setPos(20,40,300,160);
    SpdMeter.setColor(GREEN);

    uint8_t vel_max_setting = 0;
    DataMem::read(MEM_VEL_MAX, &vel_max_setting);
    uint16_t maxvel = 180;
    switch(vel_max_setting){
    case 0:
        maxvel = 60;
        break;
    case 1:
        maxvel = 180;
        break;
    case 2:
        maxvel = 240;
        break;
    case 3:
        maxvel = 320;
        break;
    case 4:
        maxvel = 380;
        break;
    case 5:
        maxvel = 65535;
        break;
    default:
        maxvel = 240;
        break;
    }
    SpdMeter.setMax(maxvel);

    return 0;
}

/* メニューを抜けるときの処理 */
int PageIndicateMeter::exitProc(void){

    /* OBDデータ受信登録解除 */
    obd.unregisterPid(spd);

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageIndicateMeter::updateProc(Page** next_page){

    // 状態遷移
    // left
    if(button[0]->wasPressed()){
        *next_page = &page_indicate;   /* 標準画面 */
        return Page::e_state::NEXT;
    }
    
    // center
    if(button[1]->wasPressed()){
        *next_page = &page_menu;   /* 設定画面へ */
        return Page::e_state::NEXT;
    }

    // right
    if(button[2]->wasPressed()){
        *next_page = &page_indicate;   /* 標準画面 */
        return Page::e_state::NEXT;
    }


    /* 表示 */
    static uint16_t spd_disp = 0;
    
    /* データ取得 */
    if(indicator.isAvailable()){
        /* 速度 */
        spd_disp = spd->at(0);
    }


    /* 描画 */
    //#define TEST
    #if defined(TEST)
    static bool dir = 0;
    if(dir){
        if((0+1000)<=spd_disp){
            spd_disp-=1000;
        }else{
            dir = 0;
        }
    }else{
        if((65535-800)>=spd_disp){
            spd_disp+=800;
        }else{
            dir = 1;
        }
    }
    #endif

    SpdMeter.setVal(spd_disp);
    SpdMeter.draw();



    return CONT;
}
