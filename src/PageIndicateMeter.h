/* GUI */

//#pragma once
#ifndef PAGE_INDICATE_METER_H
#define PAGE_INDICATE_METER_H

#include "Page.h"
#include "drawitem_spdmeter.h"
#include "elm327.h"

/*  */
class PageIndicateMeter:public Page{
public:
    PageIndicateMeter(const char* title = "LAP TIMER");
    ~PageIndicateMeter(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);   /* こっちを継承 */

private:
    /* 描画図形 */
    meter_spd SpdMeter;

    /* データ */
    ObdData* spd;
    
};




#endif
