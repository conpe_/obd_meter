#include "PageInfo.h"
#include <M5Stack.h>

#include "PageObj.h"


PageInfo::PageInfo(const char* title):Page(title){
    
    button[0]->setLabel("");
    button[1]->setLabel("BACK");
    button[2]->setLabel("OFF");
}

PageInfo::~PageInfo(void){

}

/* メニュー入ったときの処理 */
int PageInfo::entryProc(void){

    M5.Lcd.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, TFT_LIGHTGREY);
    updateDisp();   /* 描画更新 */

    return 0;
}

/* メニューを抜けるときの処理 */
int PageInfo::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageInfo::updateProc(Page** next_page){

    if(button[0]->wasPressed()){
    }

    if(button[1]->wasPressed()){
        return e_state::BACK;
    }

    if(button[2]->wasPressed()){
        /* 電源OFF */
        /* 電源につながっていないとき： */
        /*   powerOFF(省電力機能を有効にしてdeepSleepする) */
        /* 電源につながっているとき： */
        /*   ? */
        //if(M5.Power.isCharging()){          /* USBに繋がっている */
        //    M5.Power.setPowerVin(true);    /* USB取り外し時リセットする */
        //    M5.Power.deepSleep();
        //}else{
            M5.Power.powerOFF();
        //}
    }

    /* 選択変化 */
    updateDisp();   /* 描画更新 */
    
    return CONT;
}

/* 描画更新 */
void PageInfo::updateDisp(void){
    
    //M5.Lcd.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, TFT_LIGHTGREY);

    M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
    
    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(1);
    
    M5.Lcd.setCursor(0, 42);
    M5.Lcd.printf("canControl = %d\n", M5.Power.canControl());
    M5.Lcd.printf("isChargeFull = %d\n", M5.Power.isChargeFull());
    M5.Lcd.printf("isCharging = %d\n", M5.Power.isCharging());
    M5.Lcd.printf("getBatteryLevel = %d\n", M5.Power.getBatteryLevel());
    
}

