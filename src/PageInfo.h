/* GUI */

//#pragma once
#ifndef PAGE_INFO_H
#define PAGE_INFO_H

#include "Page.h"
#include <string>

/* ページのテンプレート */
class PageInfo:public Page{
public:
    PageInfo(const char* title = "INFO");
    ~PageInfo(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);

protected:

    void updateDisp(void);  /* 描画更新 */

};


#endif
