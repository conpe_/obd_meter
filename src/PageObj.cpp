/*  */

#include "PageObj.h"

/* 計測画面 */
PageIndicate page_indicate("OBDmeter");
PageIndicateMeter page_indicate_meter("OBDmeter");

/* メニュー内容 */
PageSetSensMac page_set_sensmac("OBD2 adrs");       /* OBD2アダプタのMACアドレス設定 */
PageSetMeter page_set_meter("Meter setting");       /* 速度計表示設定 */
PageInfo page_info("Info");

/* メニュー */
/* (他のメニューオブジェクトの生成後に生成する) */
PageSelectMenuSetting page_menu_setting("Setting"); /* 設定メニュー */
PageSelectMenuTop page_menu("Menu");                /* トップメニュー */

