/* GUI template */

//#pragma once

#ifndef PAGE_OBJ_H
#define PAGE_OBJ_H

#include "PageSelectMenuTop.h"
#include "PageIndicate.h"
#include "PageIndicateMeter.h"
#include "PageSetSensMac.h"
#include "PageSelectMenuSetting.h"
#include "PageInfo.h"
#include "PageSetMeter.h"

extern PageSelectMenuTop page_menu;
extern PageIndicate page_indicate;
extern PageIndicateMeter page_indicate_meter;
extern PageInfo page_info;

// menu
extern PageSetSensMac page_set_sensmac;
extern PageSetMeter page_set_meter;
extern PageSelectMenuSetting page_menu_setting; /* 設定メニュー */

#endif
