#include "PageSelectMenu.h"

#include "PageObj.h"


PageSelectMenu::PageSelectMenu(const char* title):Page(title){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PageSelectMenu::~PageSelectMenu(void){

}

/* メニュー入ったときの処理 */
int PageSelectMenu::entryProc(void){

    updateDisp();   /* 描画更新 */

    return 0;
}

/* メニューを抜けるときの処理 */
int PageSelectMenu::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageSelectMenu::updateProc(Page** next_page){
    uint8_t sel_idx_last = sel_idx;

    if(button[0]->wasPressed()){
        if(sel_idx>0){
            --sel_idx;
        }else{
            sel_idx = menus.size()-1;
        }
    }

    if(button[1]->wasPressed()){
        *next_page = menus[sel_idx].next_page;
        return menus[sel_idx].next;
    }

    if(button[2]->wasPressed()){
        if(sel_idx < menus.size()-1){
            ++sel_idx;
        }else{
            sel_idx = 0;
        }
    }

    /* 選択変化 */
    if(sel_idx_last != sel_idx){
        updateDisp();   /* 描画更新 */
    }
    
    return CONT;
}

/* 描画更新 */
void PageSelectMenu::updateDisp(void){
    
    M5.Lcd.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, TFT_LIGHTGREY);

    /* リスト表示 */
    #define MENU_ROW 7 
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setCursor(0, 42);

    uint8_t disp_sel_row;
    uint8_t disp_offset;
    if((sel_idx < MENU_ROW/2) || (menus.size()<=MENU_ROW)){
        disp_sel_row = sel_idx;
        disp_offset = 0;
    }else if(sel_idx >= menus.size()-MENU_ROW/2){
        disp_sel_row = MENU_ROW-(menus.size()-sel_idx);
        disp_offset = menus.size() - MENU_ROW;
    }else{
        disp_sel_row = MENU_ROW/2;
        disp_offset = sel_idx - MENU_ROW/2;
    }

    for(uint8_t row=0; ((MENU_ROW>menus.size())?menus.size():MENU_ROW) > row; ++row){
        if(row<menus.size()){
            if(row == disp_sel_row){
                M5.Lcd.setTextColor(TFT_MAGENTA, TFT_LIGHTGREY);
            }else{
                M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
            }
            
            M5.Lcd.setTextFont(1);  /* 等幅フォント */
            M5.Lcd.setTextSize(3);
            M5.Lcd.printf("%s", (row == disp_sel_row)?">":" ");
            M5.Lcd.setTextFont(4);  /* きれいなフォント */
            M5.Lcd.setTextSize(1);
            M5.Lcd.printf("%s\r\n", menus[row + disp_offset].label.c_str());
        }
    }


    /* スクロールバー */
    if(menus.size()>MENU_ROW){
        M5.Lcd.fillRect(
            LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
            SCROLLBAR_W, (int)(((float)SCROLLBAR_POS_TOP + (float)SCROLLBAR_H * disp_offset / (menus.size()-MENU_ROW+1)) - SCROLLBAR_POS_TOP),
            TFT_LIGHTGREY
        );
        M5.Lcd.fillRect(
            LCD_W-SCROLLBAR_W-1, (int)((float)SCROLLBAR_POS_TOP + (float)SCROLLBAR_H * disp_offset / (menus.size()-MENU_ROW+1)), 
            SCROLLBAR_W, (int)( ((float)SCROLLBAR_H * (disp_offset+1) / (menus.size()-MENU_ROW+1)) - ((float)SCROLLBAR_H * disp_offset / (menus.size()-MENU_ROW+1)) ),
            TFT_DARKGREY
        );
        M5.Lcd.fillRect(
            LCD_W-SCROLLBAR_W-1, (int)((float)SCROLLBAR_POS_TOP + ((float)SCROLLBAR_H * (disp_offset+1) / (menus.size()-MENU_ROW+1))), 
            SCROLLBAR_W, (int)((float)SCROLLBAR_H - ((float)SCROLLBAR_H * (disp_offset+1) / (menus.size()-MENU_ROW+1))),
            TFT_LIGHTGREY
        );
    }else{
        M5.Lcd.fillRect(
            LCD_W-SCROLLBAR_W-1, SCROLLBAR_POS_TOP, 
            SCROLLBAR_W, SCROLLBAR_H,
            TFT_DARKGREY
        );
    }
}

