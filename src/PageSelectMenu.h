/* GUI */

//#pragma once
#ifndef SELECT_MENU_H
#define SELECT_MENU_H

#include "Page.h"
#include <vector>
#include <string>

/* ページのテンプレート */
class PageSelectMenu:public Page{
public:
    PageSelectMenu(const char* title = "SELECT MENU");
    ~PageSelectMenu(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);

protected:
    class MenuItem{
    public:
        MenuItem(std::string l, e_state nex, Page* n):label(l),next(nex),next_page(n){};
        MenuItem(Page* n):label(n->title),next(Page::e_state::NEXT),next_page(n){};
        MenuItem(Page* n, std::string l):label(l),next(Page::e_state::NEXT),next_page(n){};
        std::string label;
        e_state next;
        Page* next_page;
    };
    std::vector<MenuItem> menus;
    uint8_t sel_idx;

    void updateDisp(void);  /* 描画更新 */

};


#endif
