#include "PageSelectMenuSetting.h"

#include "PageObj.h"


PageSelectMenuSetting::PageSelectMenuSetting(const char* title):PageSelectMenu(title){

    menus.push_back(MenuItem("[back]", BACK, nullptr));
    menus.push_back(MenuItem(&page_set_sensmac));
    menus.push_back(MenuItem(&page_set_meter));
    
}

PageSelectMenuSetting::~PageSelectMenuSetting(void){

}


