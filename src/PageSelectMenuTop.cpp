#include "PageSelectMenuTop.h"

#include "PageObj.h"


PageSelectMenuTop::PageSelectMenuTop(const char* title):PageSelectMenu(title){

    menus.push_back(MenuItem("[back]", BACK, nullptr));
    menus.push_back(MenuItem(&page_menu_setting));
    menus.push_back(MenuItem(&page_info));
    
}

PageSelectMenuTop::~PageSelectMenuTop(void){

}


