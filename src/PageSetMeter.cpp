#include "PageSetMeter.h"
#include "PageObj.h"

#include "DataMem.h"

#define PAGE_SET_VEL_MIN mem_vel_max.Min
#define PAGE_SET_VEL_MAX mem_vel_max.Max

PageSetMeter::PageSetMeter(const char* title):Page(title){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PageSetMeter::~PageSetMeter(void){

}

/* メニュー入ったときの処理 */
int PageSetMeter::entryProc(void){

    M5.Lcd.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, TFT_LIGHTGREY);

    DataMem::read(MEM_VEL_MAX, &current_val);
    set_val = current_val;

    mode = 0;
    sel_menu = 2;

    return 0;
}

/* メニューを抜けるときの処理 */
int PageSetMeter::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageSetMeter::updateProc(Page** next_page){
    
    if(button[0]->wasPressed()){
        switch(mode){
        case 0:
            if(0<sel_menu){
                --sel_menu;
            }else{
                sel_menu = 2;
            }
            break;
        case 1:
            if(PAGE_SET_VEL_MIN<set_val){
                --set_val;
            }
            break;
        }
    }

    if(button[1]->wasPressed()){
        switch(mode){
        case 0:
            /* 選択によって確定して戻るか，確定せず戻るか */
            switch(sel_menu){
            case 0:     /* 数値 */
                mode = 1;
                break;
            case 1:     /* set */
                DataMem::write(MEM_VEL_MAX, set_val);
                return BACK;
            case 2:     /* back */
                return BACK;
            }

            break;
        case 1:
            mode = 0;
            break;
        }
    }

    if(button[2]->wasPressed()){
        switch(mode){
        case 0:
            if(2>sel_menu){
                ++sel_menu;
            }else{
                sel_menu = 0;
            }
            break;
        case 1:
            if(PAGE_SET_VEL_MAX>set_val){
                ++set_val;
            }
            break;
        }
    }


    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(3);
    /* 現在値 */
    M5.Lcd.setCursor(40, 70);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("%5d", setting2maxvel(current_val));

    M5.Lcd.setCursor(130, 70);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("->");
    
    /* 変更値 */
    M5.Lcd.setCursor(180, 70);
    /* 値変更中はマゼンタ点滅。 */
    M5.Lcd.setTextColor((0==sel_menu)?((1==mode && blink)?TFT_LIGHTGREY:TFT_MAGENTA):TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("%5d", setting2maxvel(set_val));

    /* セット */
    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(0);
    M5.Lcd.setCursor(120, 140);
    M5.Lcd.setTextColor((1==sel_menu)?TFT_MAGENTA:TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("[set]");
    /* 戻る */
    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(0);
    M5.Lcd.setCursor(110, 170);
    M5.Lcd.setTextColor((2==sel_menu)?TFT_MAGENTA:TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("[back]");


    return CONT;
}

uint16_t PageSetMeter::setting2maxvel(uint8_t setting){
    switch(setting){
    case 0:
        return 60;
        break;
    case 1:
        return 180;
        break;
    case 2:
        return 240;
        break;
    case 3:
        return 320;
        break;
    case 4:
        return 380;
        break;
    case 5:
        return 65535;
        break;
    default:
        return 240;
    }
}
