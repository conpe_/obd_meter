/* GUI */

//#pragma once
#ifndef PAGE_SET_METER_H
#define PAGE_SET_METER_H

#include "Page.h"
#include <vector>
#include <string>

/* ページのテンプレート */
class PageSetMeter:public Page{
public:
    PageSetMeter(const char* title = "SET METER");
    ~PageSetMeter(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);

protected:
    uint8_t mode;       /* 0:sel, 1:変更 */
    uint8_t sel_menu;
    uint8_t current_val;
    uint8_t set_val;

    uint16_t setting2maxvel(uint8_t setting);

};


#endif
