#include "PageSetSensMac.h"
#include "PageObj.h"

#include "DataMem.h"

PageSetSensMac::PageSetSensMac(const char* title):Page(title){
    
    button[0]->setLabel("Prev");
    button[1]->setLabel("OK");
    button[2]->setLabel("Next");
}

PageSetSensMac::~PageSetSensMac(void){

}

/* メニュー入ったときの処理 */
int PageSetSensMac::entryProc(void){

    M5.Lcd.fillRect(0, 35, LCD_W-SCROLLBAR_W-2, (LCD_H-BUTTON_H)-35-2, TFT_LIGHTGREY);

    DataMem::read(MEM_BT0, &current_val[0]);
    DataMem::read(MEM_BT1, &current_val[1]);
    DataMem::read(MEM_BT2, &current_val[2]);
    DataMem::read(MEM_BT3, &current_val[3]);
    DataMem::read(MEM_BT4, &current_val[4]);
    DataMem::read(MEM_BT5, &current_val[5]);

    memcpy(set_val, current_val, 6);

    mode = 0;
    sel_menu = 13;

    return 0;
}

/* メニューを抜けるときの処理 */
int PageSetSensMac::exitProc(void){

    return 0;
}

/* メニュー中の処理 */
Page::e_state PageSetSensMac::updateProc(Page** next_page){
    
    if(button[0]->wasPressed()){
        switch(mode){
        case 0: /* カーソル移動 */
            if(0<sel_menu){
                --sel_menu;
            }else{
                sel_menu = 13;
            }
            break;
        case 1: /* 数値変更 */
            uint8_t tmp;
            tmp = (set_val[sel_menu/2]>>((sel_menu%2)?0:4))&0x0F;

            if(0<tmp){
                --tmp;
            }else{
                tmp = 15;
            }
            
            set_val[sel_menu/2] = (set_val[sel_menu/2]& ~(0x0F<<((sel_menu%2)?0:4))) | (tmp<<((sel_menu%2)?0:4));

            break;
        }
    }

    if(button[1]->wasPressed()){
        switch(mode){
        case 0: /* カーソル移動 */
            /* 選択によって確定して戻るか，確定せず戻るか，数値変更モードに移行するか */
            switch(sel_menu){
            case 12:     /* set */
                DataMem::write(MEM_BT0, set_val[0]);
                DataMem::write(MEM_BT1, set_val[1]);
                DataMem::write(MEM_BT2, set_val[2]);
                DataMem::write(MEM_BT3, set_val[3]);
                DataMem::write(MEM_BT4, set_val[4]);
                DataMem::write(MEM_BT5, set_val[5]);
                return BACK;
            case 13:     /* back */
                return BACK;
            default:     /* 数値 */
                mode = 1;
            }

            break;
        case 1: /* 数値変更 */
            mode = 0;
            break;
        }
    }

    if(button[2]->wasPressed()){
        switch(mode){
        case 0: /* カーソル移動 */
            if(13>sel_menu){
                ++sel_menu;
            }else{
                sel_menu = 0;
            }
            break;
        case 1: /* 数値変更 */
            uint8_t tmp;
            tmp = (set_val[sel_menu/2]>>((sel_menu%2)?0:4))&0x0F;

            if(15>tmp){
                ++tmp;
            }else{
                tmp = 0;
            }
            
            set_val[sel_menu/2] = (set_val[sel_menu/2]& ~(0x0F<<((sel_menu%2)?0:4))) | (tmp<<((sel_menu%2)?0:4));

            break;
        }
    }


    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(0);
    M5.Lcd.setCursor(5, 40);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("MAC address");

    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(3);
    /* 現在値 */
    for(int i=0; 12>i; ++i){
        M5.Lcd.setCursor(20 + i*18 + i/2*8, 70);
        M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
        M5.Lcd.printf("%01X", (current_val[i/2]>>(i%2?0:4))&0x0F );
    }

    /* 変更値 */
    for(int i=0; 12>i; ++i){
        M5.Lcd.setCursor(20 + i*18 + i/2*8, 105);
        M5.Lcd.setTextColor((sel_menu==i)?((1==mode && blink)?TFT_LIGHTGREY:TFT_MAGENTA):TFT_BLACK, TFT_LIGHTGREY);
        M5.Lcd.printf("%01X", (set_val[i/2]>>(i%2?0:4))&0x0F );
    }

    /* セット */
    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(0);
    M5.Lcd.setCursor(120, 140);
    M5.Lcd.setTextColor((12==sel_menu)?TFT_MAGENTA:TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("[set]");
    /* 戻る */
    M5.Lcd.setTextFont(4);  /* きれいなフォント */
    M5.Lcd.setTextSize(0);
    M5.Lcd.setCursor(110, 170);
    M5.Lcd.setTextColor((13==sel_menu)?TFT_MAGENTA:TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.printf("[back]");


    return CONT;
}
