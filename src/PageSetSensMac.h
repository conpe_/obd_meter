/* GUI */

//#pragma once
#ifndef PAGE_SET_SENS_MAC_H
#define PAGE_SET_SENS_MAC_H

#include "Page.h"
#include <vector>
#include <string>

/* ページのテンプレート */
class PageSetSensMac:public Page{
public:
    PageSetSensMac(const char* title = "SET MAC");
    ~PageSetSensMac(void);

    /* メニュー入ったときの処理 */
    virtual int entryProc(void);
    /* メニューを抜けるときの処理 */
    virtual int exitProc(void);
    /* メニュー中の処理 */
    virtual e_state updateProc(Page** next_page);

protected:
    uint8_t mode;       /* 0:sel, 1:変更 */
    uint8_t sel_menu;   /* 0～11:MAC, 12:set, 13:back */
    uint8_t current_val[6];
    uint8_t set_val[6];

};


#endif
