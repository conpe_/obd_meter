/*******************************************************************************

*******************************************************************************/

#ifndef SERIALCMD_H
#define SERIALCMD_H

#include <Arduino.h>
#include <stdint.h>
#include "CmdSrv.h"

class SerialCmd:public CmdSrv{
public:
    SerialCmd(void):connected(false),serial(0){};   /* serial(0):USBシリアル */
    uint8_t init(void);
    void task(void);

    bool isConnected(void){return connected;};
    
private:
    uint8_t connected;
    HardwareSerial serial;
    
    /* シリアル送信 */
    uint8_t write(uint8_t* send_msg, uint8_t send_len);
};

extern SerialCmd serial_cmd;

#endif //SERIALCMD
