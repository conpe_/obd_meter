/*******************************************************************************
 * buzz
 * 概要：
 *  ブザーを制御する
 *  音階データ(音階，長さ)をバッファリングし，順に再生する。
*******************************************************************************/

#include "buzz.h"

/* 実体 */
Buzz buzz;


#define NOTE_BUFF_NUM 63            /* 音再生データバッファ */
#define BUZZ_DURATION_DEFAULT 50   /* ビープ音長さ */

/* クラス内static */
std::vector<Note> Buzz::note_buff(NOTE_BUFF_NUM);
uint16_t Buzz::write_p = 0;
uint16_t Buzz::read_p = 0;

const uint16_t Buzz::buzz_note_freq[] = {
    440, 466, 494, 523, 554, 587, 622, 659, 698, 740, 784, 831, 880, 932, 988, 1047, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661
};

Buzz::Buzz(void){

}

void Buzz::init(void){
    M5.Speaker.begin();
}

/* 1msごとに呼ぶ */
void Buzz::task(void){
    static uint16_t play_cnt = 0;
    Note note;

    uint32_t current_time = millis();
    
    if(current_time >= play_cnt){      /* 前音の再生時間経過 */
        if(0==popNoteBuff(&note)){     /* 次データあり */
            
            /* データセット */
            if((BUZZ_NOTE_NONE != note.note) && (note.note>=BUZZ_NOTE_A3) && (note.note<BUZZ_NOTE_MAX)){
                /* 音あり，音範囲OK */
                /* 指定された音階の周波数で鳴らす */
                M5.Speaker.tone(buzz_note_freq[note.note-BUZZ_NOTE_A3], note.duration);
            }else{
                /* 鳴らさない */
                M5.Speaker.mute();
            }
            
            play_cnt = current_time + note.duration;                /* 再生時間設定 */
        }else{  /* 次データなし */
            /* 停める */
            //M5.Speaker.mute();
        }
    }else{
        /* 再生中 */
    }
}

/*
 * ブザー出力
 * 引数：
 *  BUZZ_NOTE note : 音
 *  uint16_t duration : 長さ [ms]
 * 戻値：
 *  0: 成功，1: 失敗(引数おかしい)
 * 備考：
 */
bool Buzz::note(Note* noteb){
    return pushNoteBuff(noteb);
}

bool Buzz::note(BUZZ_NOTE noteb, uint16_t duration){
    Note note_tmp;
    note_tmp.note = noteb;
    note_tmp.duration = duration;
    
    return pushNoteBuff(&note_tmp);
}

bool Buzz::notes(Note* notesb, uint8_t data_len){
    int i;
    
    for(i=0; i<data_len; ++i){
        if(0!=pushNoteBuff(&notesb[i])){
            return 1;       /* バッファ追加失敗 */
        }
    }
    
    return 0;
}

bool Buzz::noteSingle(BUZZ_NOTE note){
    Note note_tmp;
    
    note_tmp.note = note;
    note_tmp.duration = BUZZ_DURATION_DEFAULT;
    
    return pushNoteBuff(&note_tmp);
}

bool Buzz::noteDouble(BUZZ_NOTE note0, BUZZ_NOTE note1){
    Note note_tmp[2];
    note_tmp[0].note = note0;
    note_tmp[0].duration = BUZZ_DURATION_DEFAULT;
    note_tmp[1].note = note1;
    note_tmp[1].duration = BUZZ_DURATION_DEFAULT;
    
    return notes(note_tmp, 2);
}

bool Buzz::pushNoteBuff(Note* note){
    int write_p_tmp = write_p;
    
    if(write_p_tmp < note_buff.size()-1){
        ++write_p_tmp;
    }else{
        write_p_tmp = 0;
    }

    if(write_p_tmp!=read_p){        /* 読み込みポインタに追いつかないなら書き込む */
        
        note_buff[write_p].note = note->note;
        note_buff[write_p].duration = note->duration;
        
        write_p = write_p_tmp;
        
        return 0;
    }else{
        return 1;
    }
}

bool Buzz::popNoteBuff(Note* note){
    if(read_p != write_p){
        note->note = note_buff[read_p].note;
        note->duration = note_buff[read_p].duration;
        
        if(read_p < note_buff.size()-1){
            ++read_p;
        }else{
            read_p = 0;
        }
        
        return 0;
    }else{
        return 1;
    }
}

bool Buzz::readNoteBuff(Note* note){
    if(read_p != write_p){
        note->note = note_buff[read_p].note;
        note->duration = note_buff[read_p].duration;
        
        return 0;
    }else{
        return 1;
    }
}

