/*******************************************************************************

*******************************************************************************/

#ifndef BUZZ_H
#define BUZZ_H

#include "M5Stack.h"
#include <vector>
using namespace std;

/* 音階 */
typedef enum{
    BUZZ_NOTE_NONE = 0,
    BUZZ_NOTE_A3 = 69,
    BUZZ_NOTE_AS3,
    BUZZ_NOTE_B3,
    BUZZ_NOTE_C4,
    BUZZ_NOTE_CS4,
    BUZZ_NOTE_D4,
    BUZZ_NOTE_DS4,
    BUZZ_NOTE_E4,
    BUZZ_NOTE_F4,
    BUZZ_NOTE_FS4,
    BUZZ_NOTE_G4,
    BUZZ_NOTE_GS4,
    BUZZ_NOTE_A4,
    BUZZ_NOTE_AS4,
    BUZZ_NOTE_B4,
    BUZZ_NOTE_C5,
    BUZZ_NOTE_CS5,
    BUZZ_NOTE_D5,
    BUZZ_NOTE_DS5,
    BUZZ_NOTE_E5,
    BUZZ_NOTE_F5,
    BUZZ_NOTE_FS5,
    BUZZ_NOTE_G5,
    BUZZ_NOTE_GS5,
    BUZZ_NOTE_MAX
} BUZZ_NOTE;

/* 音再生データ */
class Note{
public:
    BUZZ_NOTE note;     /* 音階 */
    uint32_t duration;  /* 長さ[ms] */
};


class Buzz{
public:
    Buzz(void);
    void init(void);
    void task(void);

    bool note(Note* noteb);
    bool note(BUZZ_NOTE noteb, uint16_t duration);
    bool notes(Note* notesb, uint8_t data_len);
    bool noteSingle(BUZZ_NOTE note);
    bool noteDouble(BUZZ_NOTE note0, BUZZ_NOTE note1);

private:
    static std::vector<Note> note_buff;
    static uint16_t write_p;
    static uint16_t read_p;
    static bool pushNoteBuff(Note* note);
    static bool popNoteBuff(Note* note);
    static bool readNoteBuff(Note* note);
    
    static const uint16_t buzz_note_freq[];
};


extern Buzz buzz;

#endif //BUZZER_H
