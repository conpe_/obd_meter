//#pragma once
#ifndef DRAWITEM_H
#define DRAWITEM_H

#include <stdint.h>

/* 表示アイテム */
class drawitem{
public:
    virtual void draw(void) = 0;
};



#endif
