#include <string>
#include <vector>
#include <M5Stack.h>
#include "drawitem_spdmeter.h"

#define _USE_MATH_DEFINES
#include <cmath>

meter_spd::meter_spd(void):
    redraw(1) 
{
    val[0] = 65535;
    val[1] = 65535;
    max[0] = 65535;
    max[1] = 65535;
    
};


void meter_spd::setPos(uint16_t leftup_x, uint16_t leftup_y, uint16_t width, uint16_t height){
    bool change = false;

    if(this->pos_x != leftup_x){
        this->pos_x = leftup_x;
        change = true;
    }
    if(this->pos_y != leftup_y){
        this->pos_y = leftup_y;
        change = true;
    }
    if(this->width != width){
        this->width = width;
        change = true;
    }
    
    if(this->height != height){
        this->height = height;
        change = true;
    }
    
    if(change){
        redraw = true;
    }
}
void meter_spd::setColor(uint32_t col, uint32_t bgcol){
    bool change = false;

    if(color != col){
        color = col;
        change = true;
    }
    if(bgcolor != bgcol){
        bgcolor = bgcol;
        change = true;
    }

    if(change){
        redraw = true;
    }
}
uint8_t meter_spd::setVal(uint16_t val){

    /* 枠に収める */
    if(val > max[0]){
        this->val[0] = max[0];
        return 1;
    }else{
        this->val[0] = val;
        return 0;
    }
}

void meter_spd::setMax(uint16_t max){
    this->max[0] = max;

    /* 値小さくする */
    if(val[0] > max){
        val[0] = max;
    }

    redraw = true;
}

void meter_spd::draw(void){
    const float mem_pos_rad_s = 0.0f;   /* 開始角度 */
    const float mem_pos_rad_e = M_PI;   /* 最大角度 */
    const float needle_length = 120.0f;      /* 針の長さ */
    float needle_c[2] = {pos_x + width/2.0f, pos_y+height-10.0f};     /* メータの回転中心 */
    const uint16_t needle_color = TFT_RED;


    float needle_angle = 0.0f;

    /* 盤 */

    /* 背景 */
    uint16_t bgcolor = TFT_BLACK;
    if(redraw){
        M5.Lcd.fillRect(pos_x, pos_y, width, height, bgcolor);
    }else{
        /* 前回の針を消す */
        if(val[1]!=val[0]){     /* 値変化 */
            needle_angle = val[1] * M_PI / max[1];
            drawNeedle(needle_angle, needle_c[0], needle_c[1], needle_length, bgcolor);
        }
    }


    /* メモリ(軸) */
    if(redraw){
        drawCircle(needle_c[0],needle_c[1],needle_length+20,mem_pos_rad_s-0.1,mem_pos_rad_e+0.1,TFT_WHITE);
        drawCircle(needle_c[0],needle_c[1],needle_length+19,mem_pos_rad_s-0.1,mem_pos_rad_e+0.1,TFT_WHITE);
        drawCircle(needle_c[0],needle_c[1],needle_length+18,mem_pos_rad_s-0.1,mem_pos_rad_e+0.1,TFT_WHITE);
        drawCircle(needle_c[0],needle_c[1],needle_length+17,mem_pos_rad_s-0.1,mem_pos_rad_e+0.1,TFT_WHITE);
        drawCircle(needle_c[0],needle_c[1],needle_length+16,mem_pos_rad_s-0.1,mem_pos_rad_e+0.1,TFT_WHITE);

        drawCircle(needle_c[0],needle_c[1],needle_length+8,mem_pos_rad_s-0.05,mem_pos_rad_e+0.05,TFT_WHITE);
    }

    /* 目盛り */
    drawMem(needle_c[0],needle_c[1], needle_length+15, mem_pos_rad_s, mem_pos_rad_e, TFT_WHITE);

    /* デジタル */
    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(3);
    M5.Lcd.setTextColor(TFT_BLACK, TFT_LIGHTGREY);
    M5.Lcd.setCursor(180, 140);

    /* 値によらず最大値と同じ桁数にする */
    if(redraw || val[1]!=val[0]){     /* 値変化 */
        char str[10];
        uint8_t str_len_max;
        uint8_t str_len;
        str_len_max = sprintf(str, "%d", max[0]);
        str_len = sprintf(str, "%d", val[0]);

        memset(str, 0x20, str_len_max);     /* 全体を空白文字で初期化 */
        sprintf(&str[str_len_max-str_len], "%d", val[0]);
        
        M5.Lcd.printf("%s", str);

        /* 針 */
        needle_angle = val[0] * M_PI / max[0];
        drawNeedle(needle_angle, needle_c[0], needle_c[1], needle_length, needle_color);
    }

    val[1] = val[0];
    max[1] = max[0];

    redraw = false;
}

void meter_spd::drawNeedle(float angle, uint16_t c_x, uint16_t c_y, uint16_t len, uint16_t color){

    const float needle_center_dia = 10.0f;   /* 針の中心の大きさ */

    float p1[2];
    float p2[2];
    float p3[2];

    p1[0] = c_x + (needle_center_dia/2.0f*0.6f)*cosf(angle - M_PI/2.0f);
    p1[1] = c_y + (needle_center_dia/2.0f*0.6f)*sinf(angle - M_PI/2.0f);
    p2[0] = c_x + (needle_center_dia/2.0f*0.6f)*cosf(angle + M_PI/2.0f);
    p2[1] = c_y + (needle_center_dia/2.0f*0.6f)*sinf(angle + M_PI/2.0f);
    p3[0] = c_x + (len)*cosf(angle - M_PI);
    p3[1] = c_y + (len)*sinf(angle - M_PI);

    M5.Lcd.fillTriangle(p1[0], p1[1], p2[0], p2[1], p3[0], p3[1], color);

    /* 針:中央丸 */
    M5.Lcd.fillEllipse(c_x, c_y, needle_center_dia/2.0,needle_center_dia/2.0, color);
    M5.Lcd.fillEllipse(c_x, c_y, needle_center_dia/4.0,needle_center_dia/4.0, TFT_DARKGREY);
}

/* 目盛り描画 */
void meter_spd::drawMem(uint16_t c_x, uint16_t c_y, uint16_t r, float ang_s, float ang_e, uint16_t color){
    /* 20km/hごとに太い線 */
    /* 10km/hごとに細い線 */

    /* rから内側に向かって線を引く */

    uint16_t p_s[2];
    uint16_t p_e[2];

    uint16_t div_num = 0;           /* 目盛り数 */

    uint16_t kizami = 10;
    while(div_num < 10){            /* 最大速度は50km/h以上であること */
        div_num = max[0]/kizami;    /* **km/h刻みで目盛り数が幾つになるか */
        kizami = kizami/2;
    }
    if(32 < div_num){
        div_num = 32;
    }

    const uint8_t font_width = 6;
    const uint8_t font_height = 8;
    const uint8_t font_size = 2;

    /* 速度リスト生成 */
    static std::vector<uint16_t> spd_list(div_num+1, 0);
    static std::vector<uint16_t> spd_list_ofs_x(div_num+1, 0);
    static std::vector<uint16_t> spd_list_ofs_y(div_num+1, 0);
    if(redraw || max[1]!=max[0]){     /* 目盛り最大値変化 */

        for(int i=0; i<=div_num; ++i){
            float angle_c = ang_s + i*(ang_e - ang_s)/(div_num);
            ///std::string str = std::to_string(max[0] * i / div_num);
            char str[10];
            uint8_t str_len;
            str_len = sprintf(str, "%d", max[0] * i / div_num);
            uint16_t str_width = font_width*font_size*str_len;
            uint16_t str_height = font_height*font_size;

            float l = sqrt(str_height*str_height + str_width*str_width) / 2;
            float a = atan((float)str_height/(float)str_width);
            float str_r;
            if(angle_c <= M_PI/2){
                str_r = r*0.85 - l*cosf(angle_c-a);
            }else{
                str_r = r*0.85 - l*cosf(M_PI-angle_c-a);
            }


            spd_list.at(i) = max[0] * i / div_num;
            spd_list_ofs_x.at(i) = (str_r)*cosf(angle_c - M_PI) - str_width/2;
            spd_list_ofs_y.at(i) = (str_r)*sinf(angle_c - M_PI) - str_height/2;

            /*
            char print_tmp[128];
            sprintf(print_tmp, "%d %skm, len=%d, l=%lf, a=%lf, str_r=%lf \r\n", i, str, str_len, l, a, str_r);
            Serial.print(print_tmp);
            */
        }
    }

    M5.Lcd.setTextFont(1);
    M5.Lcd.setTextSize(font_size);
    M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);

    for(int i=0; i<=div_num; ++i){
        float angle_c = ang_s + i*(ang_e - ang_s)/(div_num);
        if(redraw || max[1]!=max[0]){     /* 目盛り最大値変化 */
            if(1==i%2){     /* 10km/h (max320km/hのとき) */
                float angle = angle_c;

                p_s[0] = c_x + (r)*cosf(angle - M_PI);
                p_s[1] = c_y + (r)*sinf(angle - M_PI);

                p_e[0] = c_x + (r*0.95)*cosf(angle - M_PI);
                p_e[1] = c_y + (r*0.95)*sinf(angle - M_PI);

                M5.Lcd.drawLine(p_s[0], p_s[1], p_e[0], p_e[1], color);

            }else{          /* 20km/h (max320km/hのとき) */
                for(float angle = angle_c-0.01; angle <= angle_c+0.01; angle+=0.002){
                    p_s[0] = c_x + (r)*cosf(angle - M_PI);
                    p_s[1] = c_y + (r)*sinf(angle - M_PI);

                    p_e[0] = c_x + (r*0.9)*cosf(angle - M_PI);
                    p_e[1] = c_y + (r*0.9)*sinf(angle - M_PI);

                    M5.Lcd.drawLine(p_s[0], p_s[1], p_e[0], p_e[1], color);
                }

            }
        }

        if(redraw || val[1]!=val[0]){     /* 値変化 */
            uint8_t num_div;    /* 目盛りいくつごとに数値を書くか */
            if(div_num <= 10){
                num_div = 1;
            }else if(div_num <= 20){
                num_div = 2;
            }else{
                num_div = 4;
            }


            if(0==i%num_div){ /* 40km/h (max320km/hのとき) */
                char str[10];
                sprintf(str, "%d", spd_list.at(i));
                M5.Lcd.setCursor(c_x + spd_list_ofs_x.at(i), c_y + spd_list_ofs_y.at(i));
                M5.Lcd.printf(str);
            }
        }
    }

}

/* 円 */
void meter_spd::drawCircle(uint16_t c_x, uint16_t c_y, uint16_t r, float ang_s, float ang_e, uint16_t color){
    const uint8_t mem_div_num = 20;     /* 描画分割数 */
    float mem_s_p = ang_s;     /* 今書いてる位置[rad] */
    float mem_e_p = ang_e;     /* 今書いてる位置[rad] */
    float mem_axis_s[2] = {
        c_x + (float)(r)*cosf(mem_s_p - M_PI), 
        c_y + (float)(r)*sinf(mem_s_p - M_PI)
    };
    float mem_axis_e[2];
    
    for(int i=0; i<mem_div_num; ++i){
        mem_e_p = mem_s_p + (ang_e - ang_s)/mem_div_num;
        mem_axis_e[0] = c_x + (r)*cosf(mem_e_p - M_PI);
        mem_axis_e[1] = c_y + (r)*sinf(mem_e_p - M_PI);

        M5.Lcd.drawLine(mem_axis_s[0], mem_axis_s[1], mem_axis_e[0], mem_axis_e[1], color);

        /* 次のスタート地点 = 今回の終了地点 */
        mem_s_p = mem_e_p;
        mem_axis_s[0] = mem_axis_e[0];
        mem_axis_s[1] = mem_axis_e[1];
    }
}

