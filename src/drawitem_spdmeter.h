//#pragma once
#ifndef DRAWITEM_SPDMETER_H
#define DRAWITEM_SPDMETER_H

#include <M5Stack.h>
#include "drawitem.h"

/* スピードメータ表示 */
class meter_spd : public drawitem{
public:
    meter_spd(void);

    void setPos(uint16_t leftup_x, uint16_t leftup_y, uint16_t width, uint16_t height);
    void setColor(uint32_t col, uint32_t bgcol = LIGHTGREY);
    
    uint8_t setVal(uint16_t val);
    void setMax(uint16_t max);

    void draw(void);    
private:
    bool redraw;        /* 再描画要求 */

    /* 描画データ */
    uint16_t val[2];
    uint16_t max[2];
    uint16_t pos_x;
    uint16_t pos_y;
    uint16_t width;
    uint16_t height;
    uint16_t color;
    uint16_t bgcolor;

    void drawNeedle(float angle, uint16_t c_x, uint16_t c_y, uint16_t len, uint16_t color);
    void drawMem(uint16_t c_x, uint16_t c_y, uint16_t r, float ang_s, float ang_e, uint16_t color);
    void drawCircle(uint16_t c_x, uint16_t c_y, uint16_t r, float ang_s, float ang_e, uint16_t color);
};



#endif
