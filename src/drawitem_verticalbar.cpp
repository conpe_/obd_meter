
#include "drawitem_verticalbar.h"


bar_vertical::bar_vertical(void):
    redraw(1) 
{
    val[0] = 0;
    val[1] = 0;
    max[0] = 255;
    max[1] = 255;
    
};


void bar_vertical::setPos(uint16_t leftup_x, uint16_t leftup_y, uint16_t width, uint16_t height){
    bool change = false;

    if(this->pos_x != leftup_x){
        this->pos_x = leftup_x;
        change = true;
    }
    if(this->pos_y != leftup_y){
        this->pos_y = leftup_y;
        change = true;
    }
    if(this->width != width){
        this->width = width;
        change = true;
    }
    
    if(this->height != height){
        this->height = height;
        change = true;
    }
    
    if(change){
        redraw = true;
    }
}
void bar_vertical::setColor(uint32_t col, uint32_t bgcol){
    bool change = false;

    if(color != col){
        color = col;
        change = true;
    }
    if(bgcolor != bgcol){
        bgcolor = bgcol;
        change = true;
    }

    if(change){
        redraw = true;
    }
}
uint8_t bar_vertical::setVal(uint8_t val){
    this->val[0] = val;

    /* 枠広げる */
    if(val > max[0]){
        max[0] = val;
        
        return 1;
    }

    return 0;
}

void bar_vertical::setMax(uint8_t max){
    this->max[0] = max;

    /* 値小さくする */
    if(val[0] > max){
        val[0] = max;
    }
}

void bar_vertical::draw(void){

    if(redraw){ /* 全描画 */

        /* 枠 */
        M5.Lcd.drawRect(pos_x, pos_y, width, height, DARKGREY);

        int16_t x = pos_x + 1;
        int16_t y = pos_y + 1;
        int16_t w = width - 2;
        int16_t h = height -2;

        /* 背景 */
        if(val[0] != max[0]){
            M5.Lcd.fillRect(x, y, w, (int16_t)(h*(1-(float)val[0]/max[0])), bgcolor);
        }

        /* バー */
        if(val[0] > 0){
            M5.Lcd.fillRect(x, (int16_t)(y + h*(1-(float)val[0]/max[0])+1), w, (int16_t)(h*((float)val[0]/max[0])), color);
        }

    }else{      /* 部分描画 */
        /* 値変更部分のみ再描画するようにしたい */
        if( (val[1] != val[0]) || (max[1] != max[0])){

            /* 枠 */
            M5.Lcd.drawRect(pos_x, pos_y, width, height, DARKGREY);

            int16_t x = pos_x + 1;
            int16_t y = pos_y + 1;
            int16_t w = width - 2;
            int16_t h = height -2;

            /* 背景 */
            if(val[0] != max[0]){
                M5.Lcd.fillRect(x, y, w, (int16_t)(h*(1-(float)val[0]/max[0])), bgcolor);
            }

            /* バー */
            if(val[0] > 0){
                M5.Lcd.fillRect(x, (int16_t)(y + h*(1-(float)val[0]/max[0])+1), w, (int16_t)(h*((float)val[0]/max[0])), color);
            }
        }
    }

    val[1] = val[0];
    max[1] = max[0];

}
