//#pragma once
#ifndef DRAWITEM_VERTICALBAR_H
#define DRAWITEM_VERTICALBAR_H

#include <M5Stack.h>
#include "drawitem.h"

/* バー表示 */
class bar_vertical : public drawitem{
public:
    bar_vertical(void);

    void setPos(uint16_t leftup_x, uint16_t leftup_y, uint16_t width, uint16_t height);
    void setColor(uint32_t col, uint32_t bgcol = LIGHTGREY);
    
    uint8_t setVal(uint8_t val);
    void setMax(uint8_t max);

    void draw(void);    
private:
    bool redraw;        /* 再描画要求 */

    /* 描画データ */
    uint8_t val[2];
    uint8_t max[2];
    uint16_t pos_x;
    uint16_t pos_y;
    uint16_t width;
    uint16_t height;
    uint16_t color;
    uint16_t bgcolor;
};



#endif
