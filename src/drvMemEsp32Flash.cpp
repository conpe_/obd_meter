/*******************************************************************************
 * DataMem
 * 概要：
*******************************************************************************/
#include <M5Stack.h>
#include "drvMemEsp32Flash.h"

#include "FS.h"
#include "SPIFFS.h"


DrvMemEsp32Flash::DrvMemEsp32Flash(std::string fname, uint16_t memsize):
        filename(fname),
        mem(memsize, 0x00)
    {

}

int DrvMemEsp32Flash::begin(void){
    return begin(MEM_SIZE_DEFAULT);
}

int DrvMemEsp32Flash::begin(uint16_t memsize){
    /* 配列サイズ確保 */
    if(mem.capacity() < memsize){
        mem.reserve(memsize);
    }
    /* 配列サイズ分初期化 */
    mem.assign(mem.capacity(), 0x00);

    if(!SPIFFS.begin(1)){
        return 1;   /* failed */
    }

    File fp = SPIFFS.open(filename.c_str(), FILE_READ);
    if(fp){
        /* 読み込み */
        fp.read(mem.data(), mem.size());
        fp.close();

    }else{  /* ファイルがない */
        /* 保存時に新規作成される */
    }

    return 0;
}

int DrvMemEsp32Flash::close(void){
    int ret = 0;

    File fp = SPIFFS.open(filename.c_str(), FILE_WRITE);
    if(fp){
        fp.write(mem.data(), mem.size());
        fp.close();
    }else{
        ret = 1;
    }
    
    SPIFFS.end();

    return ret;
}

int DrvMemEsp32Flash::read(uint16_t adrs, uint8_t* data, uint8_t len){
    if(adrs+len > mem.size()){
        return 1;
    }
    
    memcpy(data, &mem[adrs], len);

    return 0;
}

int DrvMemEsp32Flash::write(uint16_t adrs, uint8_t* data, uint8_t len){
    if(adrs+len > mem.size()){
        return 1;
    }

    memcpy(&mem[adrs], data, len);
    
    return 0;
}
