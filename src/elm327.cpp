#include <M5Stack.h>
#include <string.h>

#include "elm327.h"

/*  */
elm327 obd;



//String name = "OBDII";
//String pin = "1234";


elm327::elm327(void):
  connected(false),
  SerialBT()
{

  
}


/* 接続開始 */
uint8_t elm327::begin(uint8_t address[6]){
  
	uint8_t my_mac[6];
	esp_read_mac(my_mac, ESP_MAC_BT);	/* 自身のMACアドレスを取得 */
	char my_name[20];
	/* BT検索して出る名前を設定する (XXXXはMACアドレス先頭2byte) */
	sprintf(my_name, "OBDmeter%02X%02X", my_mac[0], my_mac[1]);
	SerialBT.begin(my_name, true);		/* Master側として開始 */
	SerialBT.register_callback(&elm327::esp_spp_cb);


  memcpy(elm_address, address, 6);

  mux = xSemaphoreCreateMutex();

  return 0;
}

/* 10ms周期タスク */
void elm327::task(void){
  static uint32_t last_time = 0;

  uint32_t current_time = millis();

  connected = SerialBT.connected();

  if(connected){
		/* データ更新 */
    if(last_time+20 <= current_time){  /* 20ms経過 */
      last_time = current_time;

      xSemaphoreTake( mux, portMAX_DELAY );   /* セマフォ取得 */
      for(auto itr=pid_list.begin(); itr!=pid_list.end(); ++itr){
        /* 登録済みデータ分繰り返し */
        log_i("comu %p", *itr);
        updateSingle(itr);
      }
      xSemaphoreGive( mux );                   /* セマフォ開放 */
    }

	}else{	/* 接続待ち */
		if(last_time+2000 <= current_time){  /* 2s経過 */
      last_time = current_time;
      
			SerialBT.connect(elm_address);		/* 接続しに行く */
		}
	}


}

uint8_t elm327::get(uint16_t pid, uint8_t* ret_data, uint8_t* len){
  
    xSemaphoreTake( mux, portMAX_DELAY );   /* セマフォ取得 */
    for(auto itr=pid_list.begin(); itr!=pid_list.end(); ++itr){
      /* 登録済みデータ分繰り返し */
      if((*itr)->pid == pid){
        ObdData* data_array = &(**itr);
        int i = 0;
        /* データを突っ込む */
        for(auto itr_element=data_array->begin(); itr_element!=data_array->end(); itr_element++){
          ret_data[i] = *itr_element;
          ++i;
        }
        *len = data_array->size();

        return 0;
      }
    }
    xSemaphoreGive( mux );                   /* セマフォ開放 */

    return 1;
}

uint8_t elm327::updateSingle(std::vector<ObdData*>::iterator itr_obddata){
  uint8_t buffer[32] = {0};
  uint8_t rc_len;
  uint8_t tmp_rcv = 0;
  uint8_t tmp_byte;
  uint8_t state = 0;
  uint16_t timeout = 0;

  uint16_t pid = (*itr_obddata)->pid;
  
  /* 中途半端に受けてるデータを捨てる */
  timeout = 0;
  while(SerialBT.available()){
    SerialBT.read();

    timeout++;
    if(timeout>128){
      log_i("timeout");
      break;
    }
  }
  
  /* コマンド送る */
  uint8_t send_len;
  send_len = sprintf((char*)buffer, "%02x%02x\r\n", (uint8_t)(pid>>8), (uint8_t)(pid&0x00FF));
  SerialBT.write(buffer, send_len);
  log_i("send cmd:%s", buffer);
  
  /* コマンド受ける */

  timeout = 0;
  uint8_t rcvd = 0;
  rc_len = 0;
  send_len -= 1;  /* 送るときは^r^nだけど返ってくるのは^rだけなので引く */
  while((rc_len<send_len) || (!rcvd)){	/* 送信コマンドのループバック分 */
    delay(1);
    while(SerialBT.available()){
      tmp_rcv = SerialBT.read();
      rc_len++;
      /* 改行チェック */
      if(('\r'==tmp_rcv) && ((rc_len+2)>=send_len)){
        rcvd = 1;
      }
      /* 受信終了チェック */
      if((rc_len>=send_len) && (rcvd)){
        break;
      }
    }

    timeout++;
    if(timeout>200){
      log_i("timeout loopback");
      break;
    }
  }
  
  tmp_rcv = 0;
  rc_len = 0;
  while('\r' != tmp_rcv){  /* 完了するまで受信 */
    
    delay(1);
    
    while(SerialBT.available()){
      tmp_rcv = SerialBT.read();

      if(0xFF != aschex2byte(tmp_rcv)){
        if(0 == state){ /* 上位ビット */
          tmp_byte = aschex2byte(tmp_rcv)<<4;
          state++;
        }else if(1 == state){ /* 下位ビット */
          tmp_byte |= aschex2byte(tmp_rcv);
          state = 0;
          
          buffer[rc_len] = tmp_byte;
          rc_len++;

        }
      }
      
      log_i("%c (0x%02X)", tmp_rcv, tmp_rcv);
      
      timeout = 0;
      if('\r' == tmp_rcv){
        break;
      }
    }

    timeout++;
    if(timeout>400){
      rc_len = 0;
      log_i("timeout rcv");
      break;
    }
  }

  if(rc_len>2){
    /* データ部のみ返す */
    /* はじめ2byteはコマンド */
    if((*itr_obddata)->estlen() == (rc_len-2)){   /* 想定していた長さ */
      (*itr_obddata)->assign(&buffer[2], &buffer[rc_len]);
    }else{
      log_i("length invalid : %d", rc_len);
    }

    return 0;
  }else{
    
    /* 前回値保持 */
    log_i("no data");

    return 1;
  }
}

/* 0～15 */
uint8_t elm327::aschex2byte(unsigned char hex){
  if(hex>='0' && hex<='9'){
    return hex-'0';
  }
  if(hex>='a' && hex<='f'){
    return hex-'a'+10;
  }
  if(hex>='A' && hex<='F'){
    return hex-'A'+10;
  }

  return 0xFF;
}


/* BT通信コールバック */
void elm327::esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param){

	switch((uint8_t)event){
	case ESP_SPP_OPEN_EVT:		/* Client connection open */
		M5.Speaker.tone(1200, 50);
		break;
	case ESP_SPP_CLOSE_EVT:		/* Client connection closed */
		M5.Speaker.tone(1200, 200);
		break;
	default:
		break;
	}
				
}

