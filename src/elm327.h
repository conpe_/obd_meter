
#ifndef _ELM327_H_
#define _ELM327_H_

#include "obd2.h"
#include <stdint.h>
#include "BluetoothSerial.h"

class elm327:public Obd2{
public:
  elm327(void);
  uint8_t begin(uint8_t address[6]);
  void task(void);

  bool isConnected(void){return connected;};

  uint8_t get(uint16_t pid, uint8_t* ret_data, uint8_t* len);

private:
  bool connected;
  BluetoothSerial SerialBT;
  uint8_t elm_address[6];

  /* データ更新(通信完了まで戻らない) */
  uint8_t updateSingle(std::vector<ObdData*>::iterator itr_obddata);


  uint8_t aschex2byte(unsigned char hex);  /* 1文字変換 */

  /* コールバック */
  /* BT接続関係 */
  static void esp_spp_cb(esp_spp_cb_event_t event, esp_spp_cb_param_t *param);

};


extern elm327 obd;

#endif
