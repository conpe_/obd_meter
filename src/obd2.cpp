#include "obd2.h"

#include <algorithm>

// debug
#include <Arduino.h>

const uint8_t ObdData::pid_len[] = {
  4,4,2,2,1,1,1,1,1,1,1,1,2,1,1,1,2,1,1,1,2,2,2,2,2,2,2,2,1,1,1,2,
  4,2,2,2,4,4,4,4,4,4,4,4,1,1,1,1,1,2,2,1,4,4,4,4,4,4,4,4,2,2,2,2,
  4,4,2,2,2,1,1,1,1,1,1,1,1,2,2,4,4,1,1,2,2,2,2,2,2,2,1,1,1,2,2,1,
  4,1,1,2,5,2,5,3,7,7,5,5,5,6,5,3,9,5,5,5,5,7,7,5,9,9,7,7,9,1,1,13,
  4,21,21,5,1,10,5,5,13,41,41,7,16,1,1,5,3,5,2,3,12,0,0,0,9,9,6,4,17,4,2,9,4,9,2,9,4,4,4,
  4
};


Obd2::Obd2(void){
  mux = xSemaphoreCreateMutex();
}

ObdData* Obd2::registerPid(uint16_t pid){
  ObdData* p_obdata;

  /* 登録済みならそれを返す */
  for(auto itr=pid_list.begin(); itr!=pid_list.end(); ++itr){
    if((*itr)->pid == pid){
      return &(*(*itr));
    }
  }

  /* オブジェクト生成 */
  p_obdata = new ObdData(pid);

  /* 管理リストに追加 */
  if (nullptr != p_obdata){
    xSemaphoreTake( mux, portMAX_DELAY );   /* セマフォ取得 */
    pid_list.push_back(p_obdata);
    xSemaphoreGive( mux );                  /* セマフォ開放 */
  }
  
  log_i("0x%02X %p", pid, p_obdata);

  return p_obdata;
}

void Obd2::unregisterPid(ObdData* obddata){   /* 周期登録解除 */

  xSemaphoreTake( mux, portMAX_DELAY );   /* セマフォ取得 */

  /* 登録されているデータを検索 */
  auto del_itr = std::find(pid_list.begin(), pid_list.end(), obddata);

  /* 消す */
  if(pid_list.end() != del_itr){    /* 要素発見 */
    log_i("0x%02X %p", (*del_itr)->pid, *del_itr);

    delete &(*(*del_itr));      /* オブジェクト削除 */
    pid_list.erase(del_itr);    /* リストから削除 */
  }

  xSemaphoreGive( mux );                  /* セマフォ開放 */

}


/* OBDデータオブジェクト */
ObdData::ObdData(uint16_t pid){
  this->pid = pid;
  reset();
}

void ObdData::reset(void){
  uint8_t len = estlen();

  if(len<1){
    len = 1;
  }
  
  this->assign(len, 0);
}

uint8_t ObdData::estlen(void){

  if((pid&0x00FF) < (sizeof(pid_len)/sizeof(pid_len[0])) ){
    return pid_len[pid&0x00FF];
  }

  return 0;
}
