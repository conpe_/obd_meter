
#ifndef _OBD2_H_
#define _OBD2_H_

#include <FreeRTOS.h>
#include <stdint.h>
#include <vector>

class ObdData;


class Obd2{
public:
  Obd2(void);

  virtual void task(void) = 0;    /* 周期処理 */
                                  /* pid_list内のデータを更新する */
  ObdData* registerPid(uint16_t pid);     /* 周期登録セット */
  void unregisterPid(ObdData* obddata);   /* 周期登録解除 */

protected:
  std::vector<ObdData*> pid_list;
  
  /* RTOS */
  xSemaphoreHandle mux;
};



/* データオブジェクト */
class ObdData : public std::vector<uint8_t>{
public:
  ObdData(uint16_t pid);
  void reset(void);
  uint8_t estlen(void);   /* 想定されるデータ長(実際に受信したわけではない) */
  uint16_t pid;
private:
  static const uint8_t pid_len[];
};




#endif
